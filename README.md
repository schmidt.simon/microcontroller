Some random microcontroler experiments with
micropython on an ESP32


#### Important commands to connect etc

Connect:

    mpfshell ttyUSB0

or:

    picocom /dev/ttyUSB0 -b115200

New firmware? The one for ESP32 http://micropython.org/download

Run on startup? /main.py is ran automatically (after boot.py)

    ampy --port /dev/ttyUSB0 put main.py /main.py

Drop startup script?

    ampy --port /dev/ttyUSB0 rm /main.py


### Projects


#### [Thermometer](/temp_sensor)


Use a one-wire protocol to talk to DS18B20 sensor.

(Which micropython includes a driver for)

Optionaly runs webserver with the data and posts the data to opentsdb,
to set that up just run the compose file and connect to grafana with
`admin:admin`, add new data source, then add new dashboard, should
be obvious.

![thermometer](temp_sensor/wiring.jpg)


#### [Morse code](display_morse)

First attemps at using the ESP32,
displays some morse code and uses some insane ways
to multitask, would not recommend


#### [Hall switch](hall_switch)

US2881EUA latching hall switch.


#### [MPU6050 gyro](gyro_mpu6050)

MPU6050 gyro, reading values, wip


### [UART ping](uart_ping)

Run together with the stm32 board, example `uart_ping`.


### [STM8S003F3 dev board](stm8s003f3_eval)

Dev board for stm8s003f3

![schematic](stm8s003f3_eval/schematic.png)

Pin16: GND
Pin17: 5V
Pin14: NRST  - put external pullup and cap
Pin8: SWDIO for programming

PIN1: PB5
PIN2: PB4
PIN3: PC3
PIN4: PC4
PIN5: PC5
PIN6: PC6
PIN7: PC7
PIN8: PD1
PIN9: PD2
PIN10: PD3
PIN11: PD4
PIN12: PD5
PIN13: PD6
PIN14: NRST
PIN15: N/C
PIN16: GND
PIN17: VDD
