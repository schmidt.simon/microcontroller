# Based on stuff from
# https://lauri.xn--vsandi-pxa.com/2017/06/espressif.html

from time import sleep, sleep_ms
from machine import Pin, I2C
from ssd1306 import SSD1306_I2C
import _thread

i2c = I2C(-1, Pin(4), Pin(5), freq=400000)  # Bitbanged I2C bus
oled = SSD1306_I2C(128, 64, i2c)
oled.invert(0)  # White text on black background
oled.contrast(255)  # Maximum contrast
oled.fill(0)

OFF = 1
ON = 0

led = {
    'red': Pin(12, Pin.OUT),
    'green': Pin(13, Pin.OUT),
    'blue': Pin(15, Pin.OUT),
}


def leds_off():
    for pin in led.values():
        pin.value(OFF)


# On by default on bootup, turn them off
leds_off()


def forever():
    return True


def ntimes(n):
    # Eh? next(iterable, default) doesns't work...

    x = (True for _ in range(n))

    def count():
        # return next(x, False)
        try:
            next(x)
            return True
        except StopIteration:
            return False

    return count


def cycle(*stuff):
    while True:
        for thing in stuff:
            yield thing


def blink(*pins, time_on=0.1, time_between=None, condition=forever):
    if time_between is None:
        time_between = time_on

    pins = cycle(*pins)

    while condition():
        pin = next(pins)
        pin.value(ON)
        sleep(time_on)
        pin.value(OFF)
        sleep(time_between)


def scroll_text(text, condition=forever, refresh_ms=100):
    j = 0
    while condition():
        j += 1
        oled.fill(0)
        oled.text(text[j % len(text):] + text, 10, 10)
        oled.show()
        sleep_ms(refresh_ms)

    oled.fill(0)
    oled.show()


# Private to stop_on_firts
# switched to False when any condition function
# returns False.
_running = True


def stop_on_first(*cmds):
    """
    cmds:
      (function, args, kwargs)

    Assumes kwargs contains 'condition' of type Callable[[], bool]
    that once it returns False means the function either is about to stop
    or has stopped.
    """
    global _running
    _running = True

    def wrap_condition(condition):
        """
        Modifiy condition function such that
        when _running is False it returns False,
        and if the condition itself returns False then
        set _running to False. (cascading to the other wrapped conditions)
        """
        def wrapped():
            global _running

            if _running and condition():
                return True

            _running = False
            return False

        return wrapped

    def wrapped_func(f, args, kwargs):
        def wrapped():
            return f(*args, **kwargs)

        return wrapped

    for f, args, kwargs in cmds:
        kwargs['condition'] = wrap_condition(kwargs['condition'])
        _thread.start_new_thread(wrapped_func(f, args, kwargs), ())


stop_on_first(
    (blink, tuple(led.values()), {'time_on': 0.5, 'condition': ntimes(10)}),
    (scroll_text, ('hello this is dog ', ), {'condition': forever})
)
