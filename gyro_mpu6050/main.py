import machine
from machine import Pin, I2C
import ustruct
import time

scl_pin = 23
sda_pin = 22

def twos_comp(val, bits):
    """compute the 2's complement of int value val"""
    if (val & (1 << (bits - 1))) != 0:
        val = val - (1 << bits)
    return val

SELF_TEST_X = 0x0d
POWER_MGMT_1 = 0x6b
GYRO_CONFIG = 0x1b
ACCEL_CONFIG = 0x1c

ACCEL_XOUT_H = 0x3b
# ... many readings follow this one


class Gyro:
    def __init__(self, i2c, address):
        self.i2c = i2c
        self.address = address
        self.data = bytearray(14)
        self.setup()

    def setup(self):
        self.i2c.writeto_mem(self.address, POWER_MGMT_1, b"\x00")

        # accel and gyro configs are adjacent
        gyro_config, accel_config = ustruct.unpack(
            "<BB", self.i2c.readfrom_mem(self.address, GYRO_CONFIG, 2)
        )

        # TODO maybe endian wrong
        fs_sel = (gyro_config & 24) >> 3
        self.gyro_range = 250 << fs_sel

        afs_sel = (accel_config & 24) >> 3
        self.accel_range = 1 << (afs_sel + 1)

    def _rescale_accel(self, v):
        v = twos_comp(v, 16) / (1 << 15)
        return self.accel_range * v

    def _rescale_gyro(self, v):
        v = twos_comp(v, 16) / (1 << 15)
        return self.gyro_range * v

    def _rescale_temp(self, v):
        return v / 340 + 36.5

    def read(self):
        self.i2c.readfrom_mem_into(self.address, ACCEL_XOUT_H, self.data)

        ax, ay, az, temp, gx, gy, gz = ustruct.unpack(">HHHhHHH", self.data)

        ax = self._rescale_accel(ax)
        ay = self._rescale_accel(ay)
        az = self._rescale_accel(az)

        gx = self._rescale_gyro(gx)
        gy = self._rescale_gyro(gy)
        gz = self._rescale_gyro(gz)

        temp = self._rescale_temp(temp)

        return (ax, ay, az, temp, gx, gy, gz)

    def self_test(self):
        # Enable self-test
        self.i2c.writeto_mem(self.address, ACCEL_CONFIG, b'\xf0')
        self.i2c.writeto_mem(self.address, GYRO_CONFIG, b'\xe0')

        # Wait for self test to complete
        time.sleep_ms(500)

        stx, sty, stz, sta = self.i2c.readfrom_mem(self.address, SELF_TEST_X, 4)

        # Some 5 bit integers spread across the place
        # TODO endianess ok?

        a_test = [
                (stx >> 3) | (sta & 0x30) >> 4,
                (sty >> 3) | (sta & 0x0c) >> 2,
                (stz >> 3) | (sta & 0x03) >> 0,
        ]

        g_test = [
                v & 0x1f
                for v in [stx, sty, stz]
        ]

        ft_g = [
                25*131 * (1.046 ** (test_val)) if test_val != 0 else 0
                for test_val in g_test
        ]
        ft_g[1] = -ft_g[1]  # y is negative

        def calc_ft_a(v):
            exponent = (v - 1)/(2**5 - 2)
            return 4096 * 0.34 * ((0.92/0.34) ** exponent)
        ft_a = [calc_ft_a(test_val) for test_val in a_test]

        def change(str_, ft):
            return (str_ - ft)/ft
        
        change_g = [
            change(str_, ft)
            for (str_, ft) in zip(g_test, ft_g)
        ]
        change_a = [
            change(str_, ft)
            for (str_, ft) in zip(a_test, ft_a)
        ]

        return [change_g, change_a]


class CalibratedGyro:
    def __init__(self, gyro, offsets=(0, 0, 0, 0, 0, 0, 0)):
        """
        Offsets:
            ax, ay, az, temp, gx, gy, gz
        """
        self.gyro = gyro
        self.offsets = offsets

    def read(self):
        return [
            v + o
            for v, o in zip(self.gyro.read(), self.offsets)
        ]

class TrackingGyro:
    def __init__(self, gyro):
        self.gyro = gyro
        self.max = [-100 for _ in range(7)]
        self.min = [100 for _ in range(7)]

    def read(self):
        result = self.gyro.read()

        for i, v in enumerate(result):
            if self.max[i] < v:
                self.max[i] = v
            if self.min[i] > v:
                self.min[i] = v

        return result

    def offsets(self):
        return [
            -(a + b)/2
            for (a, b) in zip(self.min, self.max)
        ]

i2c = I2C(scl=Pin(scl_pin), sda=Pin(sda_pin), freq=400000)
gyro_address = i2c.scan()[0]

gyro = Gyro(i2c, gyro_address)
gyro.read()

# Seems my device has a strange bias on the x accelerometer
calibrated_gyro = CalibratedGyro(
    gyro,
    (-0.3178, 0, 0, 0, 0, 0, 0)
)

calibrated_gyro.read()

