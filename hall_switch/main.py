import machine
import micropython
import time
import network
from machine import Pin
import esp32
import ssd1306
import urequests
import ntptime

try:
    BOOT_CONFIG = CONFIG
except KeyError:
    BOOT_CONFIG = {}

DEFAULT_CONFIG = {
    "HALL_SWITCH_PIN": 14,
    "WIFI_NAME": b"TODO",
    "WIFI_PASSWORD": "TODO",
    "TODO_PUSHDATA.IO": {"USER": "....", "API_KEY": "..", "TSNAME": "hall"},
}
CONFIG = DEFAULT_CONFIG
CONFIG.update(BOOT_CONFIG)

HALL_PIN = Pin(14, Pin.IN, Pin.PULL_DOWN)
i2c = machine.I2C(-1, machine.Pin(4), machine.Pin(5), freq=400000)  # Bitbanged I2C bus
oled = ssd1306.SSD1306_I2C(128, 64, i2c)
oled.invert(0)  # White text on black background
oled.contrast(0)  # Minimum contrast (max is 255)
oled.fill(0)

CALLBACK_STATE = HALL_PIN.value()
PUSHDATA_STATE = "idle"
PUSHDATA_QUEUE = []
PUSHDATA_DATEFORMAT = (
    "{year}-{month:02d}-{day:02d}T{hours:02d}:{minutes:02d}:{seconds:02d}.{subseconds}Z"
)
LAPS = 0
STAY_AWAKE = True
DEBOUNCE = time.ticks_ms()
INTERNET_ACTIVE = False
RTC = machine.RTC()
WLAN_IF = network.WLAN(network.STA_IF)


def connect_wifi():
    global INTERNET_ACTIVE

    print("Connecting to wifi")
    WLAN_IF.active(True)
    WLAN_IF.connect(CONFIG["WIFI_NAME"], CONFIG["WIFI_PASSWORD"])

    for _ in range(5):
        if WLAN_IF.isconnected():
            break
        time.sleep(1)

    if not WLAN_IF.isconnected():
        WLAN_IF.active(False)
        raise Exception("Unable to connect to wifi")

    ntptime.settime()
    INTERNET_ACTIVE = True
    update_screen()


def show_text(rows):
    oled.fill(0)
    print(rows)
    for i, entry in enumerate(rows.items()):
        key, value = entry
        row = "%s: %s" % (key, value)
        oled.text(row, 0, i * 10)
    oled.show()


def handle_change(dummy):
    global CALLBACK_STATE
    global LAPS
    global DEBOUNCE
    global STAY_AWAKE
    global PUSHDATA_QUEUE

    STAY_AWAKE = True

    tick = time.ticks_ms()
    if time.ticks_diff(tick, DEBOUNCE) < 25:
        return
    DEBOUNCE = tick

    new_state = HALL_PIN.value()

    if CALLBACK_STATE != new_state:
        add_to_pushdata_queue(RTC.datetime(), new_state)

    if CALLBACK_STATE == 0 and new_state == 1:
        LAPS += 1

    CALLBACK_STATE = new_state
    update_screen()


def update_screen(dummy=0):
    show_text(
        dict(
            Laps=LAPS,
            State=CALLBACK_STATE,
            Internet=INTERNET_ACTIVE,
            P_io=PUSHDATA_STATE,
        )
    )


def add_to_pushdata_queue(timetuple, value):
    if "PUSHDATA.IO" not in CONFIG:
        return
    time_data = dict(
        zip(
            (
                "year",
                "month",
                "day",
                "weekday",
                "hours",
                "minutes",
                "seconds",
                "subseconds",
            ),
            timetuple,
        )
    )
    PUSHDATA_QUEUE.append((PUSHDATA_DATEFORMAT.format(**time_data), value))


def update_pushdata():
    global PUSHDATA_STATE
    global PUSHDATA_QUEUE

    conf = CONFIG.get("PUSHDATA.IO", None)
    if not conf:
        PUSHDATA_STATE = "disabled"
        return

    if not PUSHDATA_QUEUE:
        return

    if PUSHDATA_STATE == "pending":
        return

    if not WLAN_IF.isconnected():
        return

    records = PUSHDATA_QUEUE
    PUSHDATA_QUEUE = []

    PUSHDATA_STATE = "pending (%s)" % len(records)
    update_screen()
    url = "https://pushdata.io/api/timeseries?apikey=%s" % conf["API_KEY"]

    data = {
        "name": conf["TSNAME"],
        "points": [{"time": timestamp, "value": value} for timestamp, value in records],
    }

    try:
        response = urequests.post(url, json=data)
        PUSHDATA_STATE = response.status_code
        response.close()
    except Exception as exc:
        PUSHDATA_STATE = str(exc)
        PUSHDATA_QUEUE.extend(records)
    finally:
        update_screen()


def callback(dummy):
    micropython.schedule(handle_change, 0)


def sleep_until_change():
    # We need to switch which one wakes us up next time
    value = HALL_PIN.value()
    trigger = Pin.WAKE_LOW if value else Pin.WAKE_HIGH
    HALL_PIN.irq(trigger=trigger, wake=machine.SLEEP | machine.DEEPSLEEP)
    machine.sleep()


def start():
    global STAY_AWAKE
    update_screen()
    connect_wifi()

    while True:
        # Only stay awake if something changed while we waited
        STAY_AWAKE = False
        time.sleep_ms(2000)
        if STAY_AWAKE:
            continue

        update_pushdata()
        # Sleep not really working yet, need to reliably detect if wifi is not only
        # active but also functional
        # sleep_until_change()


# Can't wake on these, but they seem to still be triggered
HALL_PIN.irq(
    trigger=Pin.IRQ_RISING | Pin.IRQ_FALLING,
    handler=callback,
    wake=machine.SLEEP | machine.DEEPSLEEP,
)

start()
