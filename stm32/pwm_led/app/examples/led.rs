#![no_main]
#![no_std]

#[allow(unused_extern_crates)]
extern crate panic_halt;

use cortex_m::peripheral::syst::SystClkSource;
use cortex_m_rt::entry;
use cortex_m_semihosting::hprint;
use stm32f1::stm32f103;

#[entry]
fn main() -> ! {
    let cortex_peripherals = cortex_m::Peripherals::take().unwrap();
    let stm32f1_peripherals = stm32f103::Peripherals::take().unwrap();

    let rcc = &stm32f1_peripherals.RCC;
    rcc.apb2enr.modify(|_, w| w.iopaen().set_bit());

    let gpioa = &stm32f1_peripherals.GPIOA;
    gpioa.moder.modify(|_, w| w.moder0().output());

    loop {}
}
