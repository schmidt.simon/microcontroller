#![no_main]
#![no_std]

#[allow(unused_extern_crates)]
extern crate panic_halt;

use cortex_m::peripheral::syst::SystClkSource;
use cortex_m_rt::entry;
use cortex_m_semihosting::hprint;
use stm32f1xx_hal::{pac, prelude::*};

#[entry]
fn main() -> ! {
    let cp = cortex_m::Peripherals::take().unwrap();
    let dp = pac::Peripherals::take().unwrap();

    let mut flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();

    let mut gpioa = dp.GPIOA.split(&mut rcc.apb2);

    let mut led = gpioa.pa0.into_push_pull_output(&mut gpioa.crl);
    let mut toggler = gpioa.pa1.into_pull_down_input(&mut gpioa.crl);

    led.set_high();

    let mut is_high = false;
    let mut state = toggler.is_high();
    let mut new_state = state;
    loop {
        new_state = toggler.is_high();
        if (state != new_state) {
            state = new_state;

            if (state) {
                led.set_high();
            } else {
                led.set_low();
            }
        }
    }
}
