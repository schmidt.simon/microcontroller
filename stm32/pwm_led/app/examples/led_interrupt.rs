#![no_main]
#![no_std]

#[allow(unused_extern_crates)]
extern crate panic_halt;

// use cortex_m::peripheral::syst::SystClkSource;
use cortex_m_rt::entry;
// use cortex_m_semihosting::hprint;
use stm32f1xx_hal::{device, pac, pac::interrupt, prelude::*, stm32::exti::PR};

use core::cell::RefCell;
use stm32f1xx_hal::gpio::gpioa::PA0;
use stm32f1xx_hal::gpio::{Output, PushPull};

static mut LED: Option<PA0<Output<PushPull>>> = None;
static mut PENDING_REGISTER: Option<PR> = None;

/*
 * To configure the 20 lines as interrupt sources, use the following procedure:
• Configure the mask bits of the 20 Interrupt lines (EXTI_IMR)
• Configure the Trigger Selection bits of the Interrupt lines (EXTI_RTSR and
EXTI_FTSR)
• Configure the enable and mask bits that control the NVIC IRQ channel mapped to the
External Interrupt Controller (EXTI) so that an interrupt coming from one of the 20 lines
can be correctly acknowledged.

Hardware event selection
To configure the 20 lines as event sources, use the following procedure:
• Configure the mask bits of the 20 Event lines (EXTI_EMR)
• Configure the Trigger Selection bits of the Event lines (EXTI_RTSR and EXTI_FTSR)
*/

#[entry]
fn main() -> ! {
    let mut cp = cortex_m::Peripherals::take().unwrap();
    let dp = pac::Peripherals::take().unwrap();

    let _flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();

    let mut gpioa = dp.GPIOA.split(&mut rcc.apb2);

    let led_gpio = gpioa.pa0.into_push_pull_output(&mut gpioa.crl);
    unsafe {
        LED = Some(led_gpio);
    }

    dp.EXTI.imr.write(|w| w.mr1().set_bit());
    dp.EXTI.rtsr.write(|w| w.tr1().set_bit());
    let _toggler = gpioa.pa1.into_pull_down_input(&mut gpioa.crl);

    cp.NVIC.enable(pac::Interrupt::EXTI1);

    loop {
        cortex_m::asm::wfi();
    }
}

#[interrupt]
fn EXTI1() {
    cortex_m::interrupt::free(|cs| {
        let exti = unsafe { &*stm32f1xx_hal::stm32::EXTI::ptr() };
        let led = unsafe { LED.as_mut().unwrap() };

        // Clear pending register, by writting a 1 to it apparently (what?)
        exti.pr.write(|w| w.pr1().set_bit());

        if (led.is_set_high()) {
            led.set_low();
        } else {
            led.set_high();
        }
    })
}
