#![no_main]
#![no_std]

#[allow(unused_extern_crates)]
extern crate panic_halt;

use cortex_m_rt::entry;
use stm32f1xx_hal::{
    pac,
    pac::interrupt,
    prelude::*,
    pwm::{Pwm, C1},
};

static mut SHARED_PWM: Option<Pwm<pac::TIM2, C1>> = None;
static mut DUTY_IX: usize = 0;
static mut DUTY_SCALES: [u16; 4] = [1, 8, 32, 100];

/*
 * To configure the 20 lines as interrupt sources, use the following procedure:
• Configure the mask bits of the 20 Interrupt lines (EXTI_IMR)
• Configure the Trigger Selection bits of the Interrupt lines (EXTI_RTSR and
EXTI_FTSR)
• Configure the enable and mask bits that control the NVIC IRQ channel mapped to the
External Interrupt Controller (EXTI) so that an interrupt coming from one of the 20 lines
can be correctly acknowledged.

Hardware event selection
To configure the 20 lines as event sources, use the following procedure:
• Configure the mask bits of the 20 Event lines (EXTI_EMR)
• Configure the Trigger Selection bits of the Event lines (EXTI_RTSR and EXTI_FTSR)
*/

#[entry]
fn main() -> ! {
    let mut cp = cortex_m::Peripherals::take().unwrap();
    let dp = pac::Peripherals::take().unwrap();

    let mut flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();

    let clocks = rcc.cfgr.freeze(&mut flash.acr);
    let mut afio = dp.AFIO.constrain(&mut rcc.apb2);
    let mut gpioa = dp.GPIOA.split(&mut rcc.apb2);

    let led_gpio = gpioa.pa0.into_alternate_push_pull(&mut gpioa.crl);
    let mut pwm = dp
        .TIM2
        .pwm(led_gpio, &mut afio.mapr, 100.hz(), clocks, &mut rcc.apb1);
    pwm.enable();
    pwm.set_duty(pwm.get_max_duty());
    unsafe {
        SHARED_PWM = Some(pwm);
    }

    // Set PA as the input source for EXTI1  (So, PA1)
    // (This seems to be the default, but for sake of completeness)
    afio.exticr1
        .exticr1()
        .write(|w| unsafe { w.exti1().bits(0b0000) });

    // Enable EXTI1
    dp.EXTI.imr.write(|w| w.mr1().set_bit());
    // Trigger EXTI1 on raising edge
    dp.EXTI.rtsr.write(|w| w.tr1().set_bit());

    let _toggler = gpioa.pa1.into_pull_down_input(&mut gpioa.crl);

    cp.NVIC.enable(pac::Interrupt::EXTI1);

    loop {
        cortex_m::asm::wfi();
    }
}

#[interrupt]
fn EXTI1() {
    cortex_m::interrupt::free(|cs| {
        let exti = unsafe { &*stm32f1xx_hal::stm32::EXTI::ptr() };
        let pwm = (unsafe { SHARED_PWM.as_mut() }).unwrap();

        // Move to the next scaler
        let max = pwm.get_max_duty();

        let divider = unsafe {
            DUTY_IX = (DUTY_IX + 1) % DUTY_SCALES.len();
            DUTY_SCALES[DUTY_IX]
        };

        pwm.set_duty(max / divider);

        // Clear pending register, by writting a 1 to it apparently (what?)
        exti.pr.write(|w| w.pr1().set_bit());
    })
}
