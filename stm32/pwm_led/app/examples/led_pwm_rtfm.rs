#![no_main]
#![no_std]

#[allow(unused_extern_crates)]
extern crate panic_halt;

use cortex_m_semihosting::hprintln;
use stm32f1xx_hal::{
    pac,
    prelude::*,
    pwm::{Pwm, C1},
};

use rtfm::app;

// PWM frequency and scale factor for the duty cycle
const FREQUENCY: u32 = 100;
static DUTY_SCALES: [u16; 5] = [1, 8, 32, 100, 0];

/*
 * To configure the 20 lines as interrupt sources, use the following procedure:
• Configure the mask bits of the 20 Interrupt lines (EXTI_IMR)
• Configure the Trigger Selection bits of the Interrupt lines (EXTI_RTSR and
EXTI_FTSR)
• Configure the enable and mask bits that control the NVIC IRQ channel mapped to the
External Interrupt Controller (EXTI) so that an interrupt coming from one of the 20 lines
can be correctly acknowledged.

Hardware event selection
To configure the 20 lines as event sources, use the following procedure:
• Configure the mask bits of the 20 Event lines (EXTI_EMR)
• Configure the Trigger Selection bits of the Event lines (EXTI_RTSR and EXTI_FTSR)
*/

#[app(device = stm32f1xx_hal::pac)]
const APP: () = {
    static mut SHARED_PWM: Pwm<pac::TIM2, C1> = ();
    static mut SHARED_EXTI: stm32f1xx_hal::stm32::EXTI = ();
    static mut DUTY_IX: usize = 0;

    #[init]
    fn init() -> init::LateResources {
        // We have some magic:
        // core: rtfm::Peripherals
        // device: stm32f1xx_hal::pac::Peripherals

        let mut flash = device.FLASH.constrain();
        let mut rcc = device.RCC.constrain();

        let clocks = rcc.cfgr.freeze(&mut flash.acr);
        let mut afio = device.AFIO.constrain(&mut rcc.apb2);
        let mut gpioa = device.GPIOA.split(&mut rcc.apb2);

        let led_gpio = gpioa.pa0.into_alternate_push_pull(&mut gpioa.crl);
        let mut pwm = device.TIM2.pwm(
            led_gpio,
            &mut afio.mapr,
            FREQUENCY.hz(),
            clocks,
            &mut rcc.apb1,
        );

        pwm.enable();
        pwm.set_duty(pwm.get_max_duty());

        // Set PA as the input source for EXTI1  (So, PA1)
        // (This seems to be the default, but for sake of completeness)
        afio.exticr1
            .exticr1()
            .write(|w| unsafe { w.exti1().bits(0b0000) });

        // Enable EXTI1
        device.EXTI.imr.write(|w| w.mr1().set_bit());
        // Trigger EXTI1 on raising edge
        device.EXTI.rtsr.write(|w| w.tr1().set_bit());

        let _toggler = gpioa.pa1.into_pull_down_input(&mut gpioa.crl);

        // Seems this is handled by rtfm
        // core.NVIC.enable(pac::Interrupt::EXTI1);

        init::LateResources {
            SHARED_PWM: pwm,
            SHARED_EXTI: device.EXTI,
        }
    }

    #[interrupt(resources = [SHARED_PWM, SHARED_EXTI, DUTY_IX])]
    fn EXTI1() {
        hprintln!("Reached exti1").unwrap();
        *resources.DUTY_IX = (*resources.DUTY_IX + 1) % DUTY_SCALES.len();

        let max = resources.SHARED_PWM.get_max_duty();

        let duty_scale = DUTY_SCALES[*resources.DUTY_IX];
        let duty = match duty_scale {
            0 => 0,
            _ => max / duty_scale,
        };
        resources.SHARED_PWM.set_duty(duty);

        // clear exti pending bit
        resources.SHARED_EXTI.pr.write(|w| w.pr1().set_bit());
    }
};
