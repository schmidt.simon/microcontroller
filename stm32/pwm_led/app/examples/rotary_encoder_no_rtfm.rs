#![no_main]
#![no_std]

#[allow(unused_extern_crates)]
extern crate panic_halt;

use cortex_m_rt::entry;
use cortex_m_semihosting::hprintln;
use stm32f1::stm32f103;

#[entry]
fn main() -> ! {
    let _p = cortex_m::Peripherals::take().unwrap();
    let device = stm32f103::Peripherals::take().unwrap();

    hprintln!("Here we go!").unwrap();
    let rcc = device.RCC;
    let tim4 = device.TIM4;

    rcc.apb1enr.write(|w| w.tim4en().set_bit());
    rcc.apb2enr
        .write(|w| w.afioen().set_bit().iopben().set_bit());
    tim4.arr.write(|w| w.arr().bits(0x27));
    tim4.ccmr1_output
        .write(|w| unsafe { w.cc1s().bits(1).cc2s().bits(1) });
    tim4.ccer.write(|w| w.cc1p().set_bit().cc2p().set_bit());
    tim4.smcr.write(|w| w.sms().encoder_mode_2());
    tim4.cr1.write(|w| w.cen().set_bit());
    loop {
        hprintln!("Hello {}", tim4.cnt.read().bits()).unwrap();
    }
}
