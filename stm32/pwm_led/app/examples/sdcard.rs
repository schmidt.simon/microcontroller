// Very much based on
// https://github.com/micropython/micropython/blob/master/drivers/sdcard/sdcard.py
//
// Would be nice if it instead was like:

//let sdcard = SDCard(spi, cs_pin);
//
//// Nicer names for commands
//sdcard.cmd0()
//sdcard.go_idle()
//
//// Better results
//let result = sdcard.go_idle();
//result.r1.parameter_error
//result.parameter_error
//
//// Maybe, also higher-level
//sdcard.write(start, data)
// // And commented!

#![deny(unsafe_code)]
#![no_main]
#![no_std]

extern crate nb;

use core::fmt;
use cortex_m::asm;
use cortex_m_semihosting::{hprint, hprintln};
use panic_halt as _;

use embedded_hal::{
    blocking::spi::{Transfer, Write},
    digital::v2::OutputPin,
    spi::{FullDuplex, MODE_0},
};
use stm32f1xx_hal::{
    pac,
    prelude::*,
    spi::{Error as SPIError, Spi},
};

use cortex_m_rt::entry;

static TOKEN_DATA: u8 = 0xfe;
static TOKEN_CMD25: u8 = 0xfc;
static TOKEN_STOP_TRAN: u8 = 0xfd;

pub struct CIDRegister {
    data: u128,
}

pub struct CSDRegisterV2 {
    data: u128,
}

impl CSDRegisterV2 {
    fn new(csd: u128) -> Result<CSDRegisterV2, SDError> {
        hprintln!("CSD: {:0128b}", csd).unwrap();
        match ((csd >> 126) & 0b11) as u8 {
            // CSD v2.0
            0x1 => Ok(CSDRegisterV2 { data: csd }),
            x => {
                // e.g. 0x00 would be CSD v1.0
                hprintln!("Got weird stuff: {:?}: {:?}", x, csd).unwrap();
                Err(SDError::UnknownCSD)
            }
        }
    }

    fn c_size(&self) -> u32 {
        // 22 bits between 48 and 69
        ((self.data >> 48) & ((1 << 22) - 1)) as u32
    }

    fn c_size_kb(&self) -> u32 {
        return (self.c_size() + 1) * 512;
    }
}

// Page 225
// Part1  Physical Layer Simplified Specification Ver6.00
pub struct R1 {
    result: u8,
}

#[derive(Debug)]
pub struct R2 {
    r1: R1,
    r2: u8,
    // TODO also data
}

#[derive(Debug)]
pub struct R3 {
    r1: R1,
    ocr: u32,
}

#[derive(Debug)]
pub struct R7 {
    r1: R1,
    rest: [u8; 4],
}

trait R1Response {
    fn r1(&self) -> &R1;
}

impl R1Response for R7 {
    fn r1(&self) -> &R1 {
        &self.r1
    }
}

impl R1Response for R2 {
    fn r1(&self) -> &R1 {
        &self.r1
    }
}

trait R1Result {
    fn in_idle_state(&self) -> bool;
    fn erase_reset(&self) -> bool;
    fn illegal_command(&self) -> bool;
    fn com_crc_error(&self) -> bool;
    fn erase_sequence_error(&self) -> bool;
    fn address_error(&self) -> bool;
    fn parameter_error(&self) -> bool;
    fn r1_error(&self) -> bool;
    fn is_idle(&self) -> bool;
}

impl R1Result for R1 {
    fn in_idle_state(&self) -> bool {
        self.result & 1 == 1
    }
    fn erase_reset(&self) -> bool {
        (self.result >> 1) & 1 == 1
    }
    fn illegal_command(&self) -> bool {
        (self.result >> 2) & 1 == 1
    }
    fn com_crc_error(&self) -> bool {
        (self.result >> 3) & 1 == 1
    }
    fn erase_sequence_error(&self) -> bool {
        (self.result >> 4) & 1 == 1
    }
    fn address_error(&self) -> bool {
        (self.result >> 5) & 1 == 1
    }
    fn parameter_error(&self) -> bool {
        (self.result >> 6) & 1 == 1
    }
    fn r1_error(&self) -> bool {
        let result = self.result | 1 != 1;
        if result {
            hprintln!("R1: {:?}", result).unwrap();
        }
        result
    }
    fn is_idle(&self) -> bool {
        self.result & 1 == 1
    }
}

impl<T> R1Result for T
where
    T: R1Response,
{
    fn in_idle_state(&self) -> bool {
        self.r1().in_idle_state()
    }
    fn erase_reset(&self) -> bool {
        self.r1().in_idle_state()
    }
    fn illegal_command(&self) -> bool {
        self.r1().illegal_command()
    }
    fn com_crc_error(&self) -> bool {
        self.r1().com_crc_error()
    }
    fn erase_sequence_error(&self) -> bool {
        self.r1().erase_sequence_error()
    }
    fn address_error(&self) -> bool {
        self.r1().address_error()
    }
    fn parameter_error(&self) -> bool {
        self.r1().parameter_error()
    }
    fn r1_error(&self) -> bool {
        self.r1().r1_error()
    }
    fn is_idle(&self) -> bool {
        // TODO might have other things going on
        self.r1().is_idle()
    }
}

impl fmt::Debug for R1 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.result == 0 {
            return write!(f, "R1 {{ result: 0 }}");
        }
        write!(
            f,
            "R1 {{
            result: {},
            in_idle_state: {},
            erase_reset: {},
            illegal_command: {},
            com_crc_error: {},
            erase_sequence_error: {},
            address_error: {},
            parameter_error: {},
            r1_error: {},
            }}",
            self.result,
            self.in_idle_state(),
            self.erase_reset(),
            self.illegal_command(),
            self.com_crc_error(),
            self.erase_sequence_error(),
            self.address_error(),
            self.parameter_error(),
            self.r1_error(),
        )
    }
}

impl R2 {
    pub fn card_is_locked(&self) -> bool {
        self.r2 & 1 == 1
    }
    pub fn wp_erase_skip_or_lock_unlock_failed(&self) -> bool {
        (self.r2 >> 1) & 1 == 1
    }
    pub fn r2_error(&self) -> bool {
        (self.r2 >> 2) & 1 == 1
    }
    pub fn cc_error(&self) -> bool {
        (self.r2 >> 3) & 1 == 1
    }
    pub fn card_ecc_failed(&self) -> bool {
        (self.r2 >> 4) & 1 == 1
    }
    pub fn wp_violation(&self) -> bool {
        (self.r2 >> 5) & 1 == 1
    }
    pub fn erase_param(&self) -> bool {
        (self.r2 >> 6) & 1 == 1
    }
    pub fn out_of_range_or_csd_overwrite(&self) -> bool {
        (self.r2 >> 7) & 1 == 1
    }
}

#[derive(Debug)]
pub enum SDError {
    // TODO split this up into more useful pieces
    // e.g. handle R1 separetely, allow preserving R1
    // Command timed out
    CommandTimeout,
    R1Error,
    NoCard,
    UnknownVersion,
    VoltageNotSupported,
    BlocklengthNotSupported,
    UnknownCSD,
    NotMultipleOfBlocklength,
    NotExactlyBlocklength,
    DataResponseTokenError,

    // TODO don't need these, error and debug traits missing all over the place..
    TransferError,
    WouldBlock,
    NOERROR, // OutputPin trait v2 shim error type, TODO useless
}

impl From<SPIError> for SDError {
    fn from(err: SPIError) -> Self {
        hprintln!("{:?}", err).unwrap();
        SDError::TransferError
    }
}

impl From<nb::Error<SPIError>> for SDError {
    fn from(err: nb::Error<SPIError>) -> Self {
        match err {
            nb::Error::WouldBlock => SDError::WouldBlock,
            nb::Error::Other(err) => {
                hprintln!("Converted nb::Error::Other {:?}", err).unwrap();
                SDError::TransferError
            }
        }
    }
}

impl From<()> for SDError {
    fn from(_err: ()) -> Self {
        return SDError::NOERROR;
    }
}

trait SpiConvenince<Word> {
    type Error;
    fn read_into(&mut self, buffer: &mut [Word], constant: Word) -> Result<(), Self::Error>;
}

impl<T, Word> SpiConvenince<Word> for T
where
    T: FullDuplex<Word>,
    Word: Clone,
{
    type Error = T::Error;
    fn read_into(&mut self, buffer: &mut [Word], constant: Word) -> Result<(), T::Error> {
        for v in buffer.iter_mut() {
            nb::block!(self.send(constant.clone()))?;
            *v = nb::block!(self.read())?;
        }
        Ok(())
    }
}

//trait SetBaudRate {
//    fn set_baudrate(self, clocks: Clocks, freq: Hertz) -> Self;
//}
//
//impl<PINS> SetBaudRate for Spi<pac::SPI1, PINS> {
//    fn set_baudrate(self, clocks: Clocks, freq: Hertz) -> Self {
//        let br = match clocks.pclk2().0 / freq.0 {
//                        0 => unreachable!(),
//                        1...2 => 0b000,
//                        3...5 => 0b001,
//                        6...11 => 0b010,
//                        12...23 => 0b011,
//                        24...47 => 0b100,
//                        48...95 => 0b101,
//                        96...191 => 0b110,
//                        _ => 0b111,
//                    };
//        return self;
//    }
//}

struct SDCard<
    SPI: Transfer<u8, Error = SPIError> + FullDuplex<u8, Error = SPIError> + Write<u8, Error = SPIError>,
    CS: OutputPin,
> {
    spi: SPI,
    cs: CS,
    cmdbuf: [u8; 6],
    tokenbuf: [u8; 1],
    is_low: bool,
}

impl<
        SPI: Transfer<u8, Error = SPIError>
            + FullDuplex<u8, Error = SPIError>
            + Write<u8, Error = SPIError>,
        CS: OutputPin<Error = ()>,
    > SDCard<SPI, CS>
{
    pub fn init(spi: SPI, cs: CS) -> SDCard<SPI, CS> {
        let cmdbuf: [u8; 6] = [0; 6];
        let tokenbuf: [u8; 1] = [0; 1];

        SDCard {
            spi: spi,
            cs: cs,
            cmdbuf: cmdbuf,
            tokenbuf: tokenbuf,
            is_low: false,
        }
    }

    pub fn cmd_r1(&mut self, cmd: u8, arg: u32, crc: u8) -> Result<R1, SDError> {
        let r1 = self.with_cs_low(|sdcard| {
            sdcard.send_cmd(cmd, arg, crc)?;
            let result = sdcard.wait_for_transmission_bit()?;
            Ok(R1 { result: result })
        })?;

        // TODO Keep an eye on this, something needs to handle it during startup
        if r1.r1_error() {
            hprintln!("R1 error: {:?}", r1).unwrap();
            return Err(SDError::R1Error);
        }
        Ok(r1)
    }

    pub fn cmd_r2(&mut self, cmd: u8, arg: u32, crc: u8) -> Result<R2, SDError> {
        self.with_cs_low(|sdcard| {
            let r1 = sdcard.cmd_r1(cmd, arg, crc)?;
            let r2 = sdcard.read_response_byte()?;
            Ok(R2 { r1: r1, r2: r2 })
        })
    }

    pub fn cmd_r7(&mut self, cmd: u8, arg: u32, crc: u8) -> Result<R7, SDError> {
        self.with_cs_low(|sdcard| {
            sdcard.send_cmd(cmd, arg, crc)?;
            let result = sdcard.wait_for_transmission_bit()?;

            // TODO use u32 instead
            let mut rest: [u8; 4] = [0xff; 4];
            sdcard.spi.transfer(&mut rest)?;
            Ok(R7 {
                r1: R1 { result: result },
                rest: rest,
            })
        })
    }

    fn cmd_r1_with_u128(&mut self, cmd: u8, arg: u32, crc: u8) -> Result<(R1, u128), SDError> {
        // Keep holding pin low after command as we will
        // retrieve the data afterwards
        self.with_cs_low(|sdcard| {
            let r1 = sdcard.cmd_r1(cmd, arg, crc)?;

            sdcard.wait_for_token_data()?;
            let mut data: u128 = 0;

            for i in 0..16 {
                let response = sdcard.read_response_byte()?;
                data |= (response as u128) << (128 - (i + 1) * 8);
            }

            Ok((r1, data))
        })
    }

    fn cmd_r3(&mut self, cmd: u8, arg: u32, crc: u8) -> Result<R3, SDError> {
        // Keep holding low and read remaining bytes right away
        // Note that the remaining bytes are not considered data, there
        // is delay possible between r1 and ocr bytes.
        // (as opposed to what readinto() does with wait_for_transmission_bit)
        self.with_cs_low(|sdcard| {
            let r1 = sdcard.cmd_r1(cmd, arg, crc)?;
            let mut ocr: u32 = 0;

            ocr &= (sdcard.read_response_byte()? as u32) << 24;
            ocr &= (sdcard.read_response_byte()? as u32) << 16;
            ocr &= (sdcard.read_response_byte()? as u32) << 8;
            ocr &= sdcard.read_response_byte()? as u32;

            Ok({ R3 { r1: r1, ocr: ocr } })
        })
    }

    pub fn cmd_go_idle_state(&mut self) -> Result<R1, SDError> {
        self.cmd_r1(0, 0, 0x95)
    }

    pub fn cmd_all_send_cid(&mut self) -> Result<(R1, CIDRegister), SDError> {
        // Number of sectors
        // CMD2: response R1 (R1 byte + 16-byte block read
        // Keep holding pin low after command as we will
        // retrieve the data afterwards
        let (r1, data) = self.cmd_r1_with_u128(10, 0, 0)?;
        Ok((r1, CIDRegister { data: data }))
    }

    pub fn cmd_send_csd(&mut self) -> Result<(R1, CSDRegisterV2), SDError> {
        // CMD9: response R1 (R1 byte + 16-byte block read
        // Keep holding pin low after command as we will
        // retrieve the data afterwards
        let (r1, data) = self.cmd_r1_with_u128(9, 0, 0)?;
        let csd = CSDRegisterV2::new(data)?;
        Ok((r1, csd))
    }

    pub fn cmd_send_status(&mut self) -> Result<R2, SDError> {
        self.cmd_r2(13, 0, 0)
    }

    pub fn cmd_send_interface_condition(&mut self) -> Result<R7, SDError> {
        // Page 74 in docs
        // VHS 4bits, 0001  = 2.7-3.6V
        // check pattern 8bits: anything
        match self.cmd_r7(8, 0x01aa, 0x87) {
            Err(SDError::CommandTimeout) => Err(SDError::VoltageNotSupported),
            Err(e) => Err(e),
            Ok(r1) => Ok(r1),
        }
    }

    pub fn cmd_set_blocklength(&mut self, blocklength: u32, crc: u8) -> Result<R1, SDError> {
        // This sets the block length
        let r1 = self.cmd_r1(16, blocklength, crc)?;
        Ok(r1)
    }

    pub fn cmd_app_cmd(&mut self) -> Result<R1, SDError> {
        // Would be neat if this state was encoded in type system
        self.cmd_r1(55, 0, 0)
    }

    pub fn cmd_read_ocr(&mut self) -> Result<R3, SDError> {
        self.cmd_r3(58, 0, 0)
    }

    pub fn acmd_sd_send_op_cond(&mut self, hcs: bool) -> Result<R1, nb::Error<SDError>> {
        self.cmd_app_cmd()?;
        let arg = match hcs {
            false => 0,
            true => 1 << 30,
        };
        // TODO crc...
        let r1 = self.cmd_r1(41, arg, 0)?;
        match r1.is_idle() {
            true => Err(nb::Error::WouldBlock),
            false => Ok(r1),
        }
    }

    pub fn cmd_read_single_block(&mut self, address: u32) -> Result<R1, SDError> {
        self.cmd_r1(17, address, 0)
    }

    pub fn read_single_block(&mut self, address: u32, buffer: &mut [u8]) -> Result<(), SDError> {
        // TODO make it obviously known compile time
        if buffer.len() != 512 {
            return Err(SDError::NotExactlyBlocklength);
        }
        self.with_cs_low(|sdcard| {
            let r1 = sdcard.cmd_read_single_block(address)?;
            sdcard.wait_for_transmission_bit()?;
            sdcard.spi.read_into(buffer, 0xff)?;
            Ok(())
        })
    }

    // What's up with spec naming things differently everywhere?
    pub fn cmd_write_single_block(&mut self, address: u32) -> Result<R1, SDError> {
        self.cmd_r1(24, address, 0)
    }

    pub fn write_single_block(&mut self, address: u32, buffer: &[u8]) -> Result<(), SDError> {
        // TODO make it obviously known compile time
        if buffer.len() != 512 {
            return Err(SDError::NotExactlyBlocklength);
        }

        let r1 = self.cmd_write_single_block(address)?;
        self.write(TOKEN_DATA, buffer)?;

        Ok(())
    }

    pub fn cmd_write_multiple_blocks(
        &mut self,
        address: u32,
        buffer: &[u8],
    ) -> Result<(), SDError> {
        // TODO make it obviously known compile time
        if buffer.len() % 512 != 0 {
            return Err(SDError::NotMultipleOfBlocklength);
        }

        self.cmd_r1(25, address, 0)?;

        for chunk in buffer.chunks(512) {
            let res = self.write(TOKEN_CMD25, chunk);
            if let Err(SDError::DataResponseTokenError) = res {
                // TODO spec is strange, should it really be cmd here and not just TOKEN_STOP_TRAN?
                hprintln!("Multi block write error, triggering cmd_stop_transmission").unwrap();
                self.cmd_stop_transmission()?;
            }
            res?;
        }

        self.with_cs_low(|sdcard| {
            sdcard.spi.write(&[TOKEN_STOP_TRAN, 0xff])?;
            sdcard.wait_for(|result| Ok(result != 0), 1000)?;
            Ok(())
        })?;

        self.spi.write(&[0xff])?;
        Ok(())
    }

    pub fn cmd_stop_transmission(&mut self) -> Result<R1, SDError> {
        self.with_cs_low(|sdcard| {
            let r1 = sdcard.cmd_r1(12, 0, 0xff)?;
            sdcard.wait_busy_end()?;
            Ok(r1)
        })
    }

    pub fn cmd_read_multiple_blocks(&mut self, address: u32) -> Result<R1, SDError> {
        self.cmd_r1(18, address, 0)
    }
    pub fn read_multiple_blocks(&mut self, address: u32, buffer: &mut [u8]) -> Result<(), SDError> {
        if buffer.len() % 512 != 0 {
            return Err(SDError::NotMultipleOfBlocklength);
        }

        let mut first = true;
        for chunk in buffer.chunks_mut(512) {
            self.with_cs_low(|sdcard| {
                if first {
                    sdcard.cmd_read_multiple_blocks(address)?;
                    first = false;
                }
                sdcard.wait_for_token_data()?;
                sdcard.spi.read_into(chunk, 0xff)?;

                // Trailed by 16bit CRC
                sdcard.read_response_byte()?;
                sdcard.read_response_byte()?;
                Ok(())
            })?
        }

        self.cmd_stop_transmission()?;
        Ok(())
    }

    fn send_cmd(&mut self, cmd: u8, arg: u32, crc: u8) -> Result<(), SDError> {
        self.cmdbuf[0] = 0x40 | cmd;
        self.cmdbuf[1] = ((arg >> 24) & 0xff) as u8;
        self.cmdbuf[2] = ((arg >> 16) & 0xff) as u8;
        self.cmdbuf[3] = ((arg >> 8) & 0xff) as u8;
        self.cmdbuf[4] = (arg & 0xff) as u8;
        self.cmdbuf[5] = crc;

        self.spi.write(&self.cmdbuf)?;
        Ok(())
    }

    pub fn read_blocks(&mut self, address: u32, buffer: &mut [u8]) -> Result<(), SDError> {
        // TODO: Block length hardcoded to 512
        if !buffer.len() % 512 == 0 {
            return Err(SDError::NotMultipleOfBlocklength);
        }

        // Read exactly one block
        if buffer.len() == 512 {
            return self.read_single_block(address, buffer);
        }

        self.read_multiple_blocks(address, buffer)
    }

    pub fn write_blocks(&mut self, address: u32, buffer: &[u8]) -> Result<(), SDError> {
        if !buffer.len() % 512 == 0 {
            return Err(SDError::NotMultipleOfBlocklength);
        }

        if buffer.len() == 512 {
            return self.write_single_block(address, buffer);
        }

        self.cmd_write_multiple_blocks(address, buffer)
    }

    fn write(&mut self, token: u8, buffer: &[u8]) -> Result<(), SDError> {
        self.with_cs_low(|sdcard| {
            sdcard.spi.write(&[token.clone()])?;

            sdcard.spi.write(buffer)?;
            sdcard.spi.write(&[0xff, 0xff])?;

            let response = sdcard.read_response_byte()?;
            if response & 0x1f != 0b101 {
                return Err(SDError::DataResponseTokenError);
            }
            sdcard.wait_for(|result| Ok(result != 0), 200)?;
            Ok(())
        })?;

        self.spi.write(&[0xff])?;

        Ok(())
    }

    fn wait_for<F>(&mut self, condition: F, maxwait: u32) -> Result<u8, SDError>
    where
        F: Fn(u8) -> Result<bool, SDError>,
    {
        for _ in 0..maxwait {
            let response = self.read_response_byte()?;

            if (condition(response)?) {
                return Ok(response);
            }
        }

        Err(SDError::CommandTimeout)
    }

    fn wait_for_transmission_bit(&mut self) -> Result<u8, SDError> {
        self.wait_for(|v| Ok((v & 0x80) == 0), 100)
    }

    fn wait_for_idle(&mut self) -> Result<(), SDError> {
        // Might need to do this a few times apparently
        let mut is_idle;
        for _ in 0..5 {
            is_idle = self
                .cmd_go_idle_state()
                .and_then(|r1| Ok(r1.is_idle()))
                .or_else(|e| match e {
                    SDError::R1Error => Ok(false),
                    e => Err(e),
                })?;
            if is_idle {
                return Ok(());
            }
        }
        return Err(SDError::NoCard);
    }

    fn wait_for_token_data(&mut self) -> Result<(), SDError> {
        self.wait_for(|response| Ok(response == TOKEN_DATA), 10)?;
        Ok(())
    }

    fn wait_busy_end(&mut self) -> Result<(), SDError> {
        self.wait_for(
            |response| {
                match response {
                    0xff => Ok(true),
                    // 0x1f => Ok(true),
                    0x00 => Ok(false),
                    // Maybe less strict?
                    v => {
                        hprintln!("Unexpected during wait_busy_end: {:?}", v).unwrap();
                        Err(SDError::TransferError)
                    }
                }
            },
            100,
        )?;
        Ok(())
    }

    fn read_response_byte(&mut self) -> Result<u8, SDError> {
        self.spi.read_into(&mut self.tokenbuf, 0xff)?;
        Ok(self.tokenbuf[0])
    }

    fn with_cs_low<F, T>(&mut self, f: F) -> Result<T, SDError>
    where
        F: FnOnce(&mut Self) -> Result<T, SDError>,
    {
        // TODO remake this to take ownership of CS pin, maybe make a custom
        //      pin trait for the with_cs_low context
        if self.is_low {
            // We are inside a with_cs_low context already, do nothing.
            return f(self);
        }

        // This is the top most context, responsible for actually setting/unsetting
        self.cs.set_low()?;
        self.is_low = true;
        let result = f(self);
        self.cs.set_high()?;
        self.is_low = false;
        result
    }

    pub fn init_card(&mut self) -> Result<(), SDError> {
        // Set chip select high
        self.cs.set_high()?;
        self.is_low = false;

        for _ in 0..16 {
            self.read_response_byte()?;
        }

        self.wait_for_idle()?;

        // TODO enable crc at some point

        // Check voltage (and implicitly version?)
        let r7 = self.cmd_send_interface_condition()?;
        if !r7.is_idle() {
            hprintln!("Received: {:?}", r7).unwrap();
            return Err(SDError::UnknownVersion);
        }

        hprintln!("Detected sd card version: 2").unwrap();
        self.init_card_v2()?;
        // Number of sectors
        // CMD9: response R1 (R1 byte + 16-byte block read
        let (_, csd) = self.cmd_send_csd()?;
        let c_size_kb = csd.c_size_kb();
        hprintln!("Available size: {:?}KByte", c_size_kb).unwrap();

        // Set block length to 512
        self.cmd_set_blocklength(512, 0)?;

        // TODO Set SPI clock rate to 1320000
        // self.spi.set_baudrate(clocks, 1320000.hz());
        Ok(())
    }

    pub fn init_card_v2(&mut self) -> Result<(), SDError> {
        self.cmd_read_ocr()?; // TODO actually confirm OCR
        nb::block!(self.acmd_sd_send_op_cond(true))?;
        self.cmd_read_ocr()?;
        Ok(())
    }

    //    pub fn cmd0(&mut self) -> Result<R1, SDError> {
    //        self.cmd_go_idle_state()
    //    }
    //
    //    pub fn cmd2(&mut self, buffer: [u8; 16]) -> Result<CIDRegister, SDError> {
    //        self.cmd_all_send_cid(buffer)
    //    }
    //
    //    pub fn cmd8(&mut self) -> Result<R7, SDError> {
    //        self.cmd_send_interface_condition()
    //    }
    //    pub fn cmd9(&mut self, buffer: [u8; 16]) -> Result<CSDRegister, SDError> {
    //        self.cmd_send_csd(buffer)
    //    }
    //    pub fn cmd16(&mut self, blocklength: u32, crc: u8) -> Result<R1, SDError> {
    //        self.cmd_set_blocklength(blocklength, crc)
    //    }
}

#[entry]
fn main() -> ! {
    let device = pac::Peripherals::take().unwrap();
    let core = cortex_m::Peripherals::take().unwrap();

    let mut flash = device.FLASH.constrain();
    let mut rcc = device.RCC.constrain();
    let clocks = rcc.cfgr.freeze(&mut flash.acr);

    // Prepare the alternate function I/O registers
    let mut afio = device.AFIO.constrain(&mut rcc.apb2);

    // Prepare the GPIO peripheral
    let mut gpioa = device.GPIOA.split(&mut rcc.apb2);

    // Set up GPIO pins
    // Pin mapping for SPI1
    let sck = gpioa.pa5.into_alternate_push_pull(&mut gpioa.crl);
    let miso = gpioa.pa6;
    let mosi = gpioa.pa7.into_alternate_push_pull(&mut gpioa.crl);
    let cs = gpioa.pa1.into_push_pull_output(&mut gpioa.crl);

    // TODO mode for speed?
    let spi = Spi::spi1(
        device.SPI1,
        (sck, miso, mosi),
        &mut afio.mapr,
        MODE_0,
        100000.hz(),
        clocks,
        &mut rcc.apb2,
    );

    hprintln!("Init card").unwrap();
    let mut sd_card = SDCard::init(spi, cs);
    match sd_card.init_card() {
        Err(e) => hprintln!("Init failed {:?}", e).unwrap(),
        Ok(_) => hprintln!("Init ok!").unwrap(),
    }

    // TODO clock up card. no embedded-hal trait for it, just recreate the spi,
    // maybe pass function (SPI, freq) -> SPI

    let mut buffer: [u8; 1024] = [0xcc; 1024];

    match sd_card.read_blocks(0, &mut buffer[0..512]) {
        Err(e) => {
            hprintln!("Unable to read block: {:?}", e).unwrap();
        }
        _ => (),
    }
    hprintln!("Read data:").unwrap();
    for v in buffer[0..10].iter() {
        hprint!("{:?},", v).unwrap();
    }
    hprintln!("...").unwrap();

    hprintln!("Start read multiple blocks").unwrap();
    match sd_card.read_blocks(0, &mut buffer) {
        Err(e) => {
            hprintln!("Unable to multi read block: {:?}", e).unwrap();
        }
        _ => (),
    }
    hprintln!("Read multiple data:").unwrap();
    for v in buffer[0..10].iter() {
        hprint!("{:?},", v).unwrap();
    }
    hprintln!("...").unwrap();
    for v in buffer[512..522].iter() {
        hprint!("{:?},", v).unwrap();
    }
    hprintln!("...").unwrap();

    hprintln!("Write single block").unwrap();
    match sd_card.write_blocks(1, &mut buffer[0..512]) {
        Err(e) => {
            hprintln!("Unable to write single block: {:?}", e).unwrap();
        }
        _ => (),
    }

    hprintln!("Write multiple blocks").unwrap();
    match sd_card.write_blocks(1, &mut buffer[0..1024]) {
        Err(e) => {
            hprintln!("Unable to write multiple blocks: {:?}", e).unwrap();
        }
        _ => (),
    }

    hprintln!("CMD send status").unwrap();
    match sd_card.cmd_send_status() {
        Err(e) => {
            hprintln!("Send status failed: {:?}", e).unwrap();
        }
        r2 => {
            hprintln!("CMD send status: {:?}", r2).unwrap();
        }
    }

    hprintln!("Done!").unwrap();
    // Trigger a breakpoint to allow us to inspect the values
    asm::bkpt();

    loop {}
}
