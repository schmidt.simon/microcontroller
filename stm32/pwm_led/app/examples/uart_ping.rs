#![no_main]
#![no_std]

#[allow(unused_extern_crates)]
extern crate panic_halt;

use cortex_m_semihosting::hprintln;
use stm32f1xx_hal::{
    pac,
    prelude::*,
    serial::{Event, Serial},
};

use rtfm::app;

/*
 * To configure the 20 lines as interrupt sources, use the following procedure:
• Configure the mask bits of the 20 Interrupt lines (EXTI_IMR)
• Configure the Trigger Selection bits of the Interrupt lines (EXTI_RTSR and
EXTI_FTSR)
• Configure the enable and mask bits that control the NVIC IRQ channel mapped to the
External Interrupt Controller (EXTI) so that an interrupt coming from one of the 20 lines
can be correctly acknowledged.

Hardware event selection
To configure the 20 lines as event sources, use the following procedure:
• Configure the mask bits of the 20 Event lines (EXTI_EMR)
• Configure the Trigger Selection bits of the Event lines (EXTI_RTSR and EXTI_FTSR)
*/

#[app(device = stm32f1xx_hal::device)]
const APP: () = {
    static mut SHARED_EXTI: stm32f1xx_hal::stm32::EXTI = ();
    static mut SHARED_TX: stm32f1xx_hal::serial::Tx<stm32f1::stm32f103::USART3> = ();
    static mut SHARED_RX: stm32f1xx_hal::serial::Rx<stm32f1::stm32f103::USART3> = ();

    #[init]
    fn init() -> init::LateResources {
        // We have some magic:
        // core: rtfm::Peripherals
        // device: stm32f1xx_hal::pac::Peripherals

        let mut flash = device.FLASH.constrain();
        let mut rcc = device.RCC.constrain();

        let clocks = rcc.cfgr.freeze(&mut flash.acr);
        let mut afio = device.AFIO.constrain(&mut rcc.apb2);
        let mut gpioa = device.GPIOA.split(&mut rcc.apb2);
        let mut gpiob = device.GPIOB.split(&mut rcc.apb2);

        let pin_tx = gpiob.pb10.into_alternate_push_pull(&mut gpiob.crh);
        let pin_rx = gpiob.pb11;

        // Create an interface struct for USART1 with 9600 Baud
        let mut serial = Serial::usart3(
            device.USART3,
            (pin_tx, pin_rx),
            &mut afio.mapr,
            9_600.bps(),
            clocks,
            &mut rcc.apb1,
        );

        serial.listen(Event::Rxne);
        let (tx, rx) = serial.split();

        // Set PA as the input source for EXTI1  (So, PA1)
        // (This seems to be the default, but for sake of completeness)
        afio.exticr1
            .exticr1()
            .write(|w| unsafe { w.exti1().bits(0b0000) });

        // Enable EXTI1
        device.EXTI.imr.write(|w| w.mr1().set_bit());
        // Trigger EXTI1 on raising edge
        device.EXTI.rtsr.write(|w| w.tr1().set_bit());

        let _toggler = gpioa.pa1.into_pull_down_input(&mut gpioa.crl);

        init::LateResources {
            SHARED_TX: tx,
            SHARED_RX: rx,
            SHARED_EXTI: device.EXTI,
        }
    }

    #[interrupt(resources = [SHARED_RX])]
    fn USART3() {
        hprintln!("Reached USART3").unwrap();

        let result = resources.SHARED_RX.read();

        match result {
            Ok(v) => hprintln!("Received {}", v).unwrap(),
            Err(_) => hprintln!("Read failed!").unwrap(),
        };
    }

    #[interrupt(resources = [SHARED_TX, SHARED_EXTI])]
    fn EXTI1() {
        hprintln!("Reached exti1").unwrap();

        let result = resources.SHARED_TX.write(b'R');

        match result {
            Ok(_) => hprintln!("Write OK",).unwrap(),
            Err(_) => hprintln!("Write failed!").unwrap(),
        };

        // clear exti pending bit
        resources.SHARED_EXTI.pr.write(|w| w.pr1().set_bit());
    }
};
