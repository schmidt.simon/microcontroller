#![no_main]
#![no_std]

#[allow(unused_extern_crates)]
extern crate panic_halt;

use cortex_m_rt::entry;
use stm32f1::stm32f103::{self, interrupt};
use stm32f1xx_hal::{flash::FlashExt, rcc::RccExt};

use cortex_m_semihosting::hprintln;

static mut BUFFER: [u16; 255] = [128; 255];
static mut SHARED_DMA: Option<stm32f103::DMA1> = None;

#[entry]
fn main() -> ! {
    let mut core = cortex_m::Peripherals::take().unwrap();
    let device = stm32f103::Peripherals::take().unwrap();
    let rcc = device.RCC;

    // Enable interupt
    core.NVIC.enable(interrupt::DMA1_CHANNEL3);
    unsafe { core.NVIC.set_priority(interrupt::DMA1_CHANNEL3, 0xa) };

    // Enable peripherals
    #[rustfmt::skip]
    rcc.ahbenr.write(|w| {
        w
            .dma1en().enabled()
    });
    #[rustfmt::skip]
    rcc.apb2enr.write(|w| {
        w
            .spi1en().enabled()
            .iopaen().enabled()
            .afioen().enabled()
    });

    let constrained_rcc = rcc.constrain();
    let mut constrained_flash = device.FLASH.constrain();
    let _clocks = constrained_rcc.cfgr.freeze(&mut constrained_flash.acr);

    // Set up GPIO pins
    // Pin mapping for SPI1
    // SCK:  PA5
    // MISO: PA6
    // MOSI: PA7
    #[rustfmt::skip]
    device.GPIOA.crl.write(|w| {
        w
            .cnf5().alt_push_pull()
            .cnf7().alt_push_pull()
            .mode5().output50() // Need at least 4Mhz
            .mode7().output50()
    });

    // SPI:
    // For unidirectional tx we want: (25.3.4)
    // BIDIMODE = 0
    // RXONLY = 0
    // MSTR = 1  , we'll use MOSI for tx

    // Enable TX DMA
    device.SPI1.cr2.write(|w| w.txdmaen().enabled());

    #[rustfmt::skip]
    device.SPI1.cr1.write(|w| {
        w
            .bidimode().unidirectional()
            .br().div16() // TODO freq = PCLK1/N figure out what's closest
            .mstr().master()
            .dff().sixteen_bit()
            .spe().enabled()
            // Meh, just trying stuff at this point
            .ssm().enabled()
            .ssi().slave_not_selected()
    });

    // 13.3.7 SPA1_TX is on DMA1 channel 3
    // See 13.3.3 For setup order, par->mar->ndtr->cr->en
    device.DMA1.ch3.par.write(|w| {
        // SPI data register, can't figure out how to use device.SPI.dr + unsafe
        w.pa().bits(0x4001300c)
    });
    let address = unsafe { BUFFER.as_ptr() as u32 };
    hprintln!("Mem address 0x{:x}", address).unwrap();
    device.DMA1.ch3.mar.write(|w| {
        // Read from BUFFER
        w.ma().bits(address)
    });
    device.DMA1.ch3.ndtr.write(|w| {
        // Meh, let's try just 1 for now
        w.ndt().bits(unsafe { BUFFER.len() } as u16)
    });

    #[rustfmt::skip]
    device.DMA1.ch3.cr.write(|w| {
        w
            // Memory to peripheral
            .mem2mem().disabled()
            .dir().from_memory()
            // SPI data register can hold 16
            .msize().bit16()
            .psize().bit16()
            // Increment along the BUFFER array
            .minc().enabled()
            // Don't increment the spi data register pointer
            .pinc().disabled()
            // Use circular buffer
            .circ().enabled()
            .en().enabled()
            // Transfer complete/error interrupt
            .htie().enabled()
            .tcie().enabled()
            .teie().enabled()
    });
    unsafe { SHARED_DMA.replace(device.DMA1) };


    // device.SPI1.cr1.modify(|_, w| w.ssi().slave_selected());

    // 25.3.9
    // DMA request issued when TXE is set to 1
    // Possible to enable only Tx DMA
    // When done, DMA_ISR.TCIF is set, can also monitor BSY

    let mut c = 0;
    loop {
        c = c + 1;
        hprintln!("\n------- {:?}", c).unwrap();

        //cortex_m::asm::wfi();
        let status = unsafe {SHARED_DMA.as_ref().unwrap()}.isr.read();
        hprintln!("DMA Err: {:?}", status.teif3()).unwrap();
        hprintln!("DMA tcif: {:?}", status.tcif3()).unwrap();
        hprintln!("DMA htif: {:?}", status.htif3()).unwrap();
        hprintln!("DMA global interrupt {:?}", status.gif3()).unwrap();

        let status = device.SPI1.sr.read();
        hprintln!("SPI TXE: {:?}", status.txe()).unwrap();
        hprintln!("SPI BSY: {:?}", status.bsy()).unwrap();
        hprintln!("SPI OVR: {:?}", status.ovr()).unwrap();
        hprintln!("SPI UDR: {:?}", status.udr()).unwrap();
        hprintln!("SPI MODF: {:?}", status.modf()).unwrap();
    }
}

#[interrupt]
fn DMA1_CHANNEL3() {
    hprintln!("DMA1 ch 3 interrupt").unwrap();

    match unsafe { SHARED_DMA.as_mut() } {
        Some(dma) => {
            let status = dma.isr.read();
            hprintln!("DMA Err: {:?}", status.teif3()).unwrap();
            hprintln!("DMA tcif: {:?}", status.tcif3()).unwrap();
            hprintln!("DMA htif: {:?}", status.htif3()).unwrap();
            hprintln!("DMA global interrupt {:?}", status.gif3()).unwrap();

            dma.ifcr.write(|w| w.cgif3().clear());
        },
        None => {
            hprintln!("SHARED_DMA unset").unwrap();
        }
    }
}
