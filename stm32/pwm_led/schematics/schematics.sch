EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED D?
U 1 1 5CD7EF82
P 4500 5000
F 0 "D?" H 4493 5216 50  0000 C CNN
F 1 "LED" H 4493 5125 50  0000 C CNN
F 2 "" H 4500 5000 50  0001 C CNN
F 3 "~" H 4500 5000 50  0001 C CNN
	1    4500 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 5200 4350 5000
$Comp
L power:GNDREF #PWR?
U 1 1 5CD80700
P 4350 5200
F 0 "#PWR?" H 4350 4950 50  0001 C CNN
F 1 "GNDREF" H 4355 5027 50  0000 C CNN
F 2 "" H 4350 5200 50  0001 C CNN
F 3 "" H 4350 5200 50  0001 C CNN
	1    4350 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5CD80E7C
P 4800 5000
F 0 "R?" V 4593 5000 50  0000 C CNN
F 1 "R" V 4684 5000 50  0000 C CNN
F 2 "" V 4730 5000 50  0001 C CNN
F 3 "~" H 4800 5000 50  0001 C CNN
	1    4800 5000
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_Push SW?
U 1 1 5CD8348D
P 4750 4200
F 0 "SW?" H 4750 4485 50  0000 C CNN
F 1 "SW_Push" H 4750 4394 50  0000 C CNN
F 2 "" H 4750 4400 50  0001 C CNN
F 3 "~" H 4750 4400 50  0001 C CNN
	1    4750 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5CD867F2
P 4750 4550
F 0 "C?" V 4521 4550 50  0000 C CNN
F 1 "C_Small" V 4612 4550 50  0000 C CNN
F 2 "" H 4750 4550 50  0001 C CNN
F 3 "~" H 4750 4550 50  0001 C CNN
	1    4750 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	4950 4200 4950 4550
Wire Wire Line
	4950 4550 4850 4550
Wire Wire Line
	4550 4200 4550 4550
Wire Wire Line
	4550 4550 4650 4550
Wire Wire Line
	4950 4200 5350 4200
Connection ~ 4950 4200
$Comp
L power:GNDREF #PWR?
U 1 1 5CD8A22B
P 5350 4650
F 0 "#PWR?" H 5350 4400 50  0001 C CNN
F 1 "GNDREF" H 5355 4477 50  0000 C CNN
F 2 "" H 5350 4650 50  0001 C CNN
F 3 "" H 5350 4650 50  0001 C CNN
	1    5350 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5CD8D98A
P 5350 4500
F 0 "R?" H 5420 4546 50  0000 L CNN
F 1 "R" H 5420 4455 50  0000 L CNN
F 2 "" V 5280 4500 50  0001 C CNN
F 3 "~" H 5350 4500 50  0001 C CNN
	1    5350 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 4200 5350 4350
Wire Wire Line
	4550 4200 4250 4200
Connection ~ 4550 4200
$Comp
L power:+3.3V #PWR?
U 1 1 5CD981D3
P 4250 4050
F 0 "#PWR?" H 4250 3900 50  0001 C CNN
F 1 "+3.3V" H 4265 4223 50  0000 C CNN
F 2 "" H 4250 4050 50  0001 C CNN
F 3 "" H 4250 4050 50  0001 C CNN
	1    4250 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 4050 4250 4200
$Comp
L MCU_ST_STM32F1:STM32F103C8Tx U?
U 1 1 5CD9BAF5
P 7200 4750
F 0 "U?" H 7150 3069 50  0000 C CNN
F 1 "STM32F103C8Tx" H 7150 3160 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 6600 3350 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00161566.pdf" H 7200 4750 50  0001 C CNN
	1    7200 4750
	-1   0    0    1   
$EndComp
Wire Wire Line
	6600 5000 6600 4950
Wire Wire Line
	4950 5000 6600 5000
Wire Wire Line
	5350 4200 6000 4200
Wire Wire Line
	6000 4200 6000 4850
Wire Wire Line
	6000 4850 6600 4850
Connection ~ 5350 4200
$EndSCHEMATC
