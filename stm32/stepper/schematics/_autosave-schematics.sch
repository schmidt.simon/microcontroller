EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Motor:Stepper_Motor_bipolar M?
U 1 1 5D22D46C
P 4950 5300
F 0 "M?" H 5138 5424 50  0000 L CNN
F 1 "Stepper_Motor_bipolar" H 5138 5333 50  0000 L CNN
F 2 "" H 4960 5290 50  0001 C CNN
F 3 "http://www.infineon.com/dgdl/Application-Note-TLE8110EE_driving_UniPolarStepperMotor_V1.1.pdf?fileId=db3a30431be39b97011be5d0aa0a00b0" H 4960 5290 50  0001 C CNN
	1    4950 5300
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:L293D U?
U 1 1 5D23292A
P 3000 5500
F 0 "U?" H 3000 6681 50  0000 C CNN
F 1 "L293D" H 3000 6590 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 3250 4750 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/l293.pdf" H 2700 6200 50  0001 C CNN
	1    3000 5500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
