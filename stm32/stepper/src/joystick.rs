//! This is based on: https://github.com/smart-leds-rs/ws2812-spi-rs
//! Needed to copy it to change timings
//!
//! This prerenders the data, so that no calculations have to be performed while sending the data.
//!
//! This approach minimizes timing issues, at the cost of much higher ram usage.
//! It also increases the needed time.

use core::fmt;
use cortex_m_semihosting::hprintln;
use embedded_hal::digital::v2::OutputPin;
use nb;
use nb::block;

// Do I need this? .abs() seems to not work
fn abs(x: f32) -> f32 {
    if x < 0.0 {
        -x
    } else {
        x
    }
}

pub struct JoyStick<F: FnMut() -> (u16, u16)> {
    pub raw_read: F,
    pub x_min: u16,
    pub x_max: u16,
    pub y_min: u16,
    pub y_max: u16,
}

impl<F: FnMut() -> (u16, u16)> fmt::Debug for JoyStick<F> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "JS {{ x: [{}, {}] y: [{}, {}] }}",
            self.x_min, self.x_max, self.y_min, self.y_max
        )
    }
}

#[derive(Debug)]
pub struct JoyStickValue {
    pub x: f32,
    pub y: f32,
}

impl JoyStickValue {
    pub fn angle(&self) -> f32 {
        // This is an approximation of atan2, but should be pretty close
        // https://math.stackexchange.com/a/1105038

        let abs_x = abs(self.x);
        let abs_y = abs(self.y);
        let a = abs_x.min(abs_y) / abs_x.max(abs_y);
        let s = a * a;
        let mut r = ((-0.0464964749 * s + 0.15931422) * s - 0.327622764) * s * a + a;
        if abs_y > abs_x {
            r = core::f32::consts::FRAC_PI_2 - r
            // 1.57079637 - r
        }
        if self.x < 0.0 {
            r = core::f32::consts::PI - r
        }
        if self.y < 0.0 {
            r = -r
        }
        r + core::f32::consts::PI
    }
}

impl<F: FnMut() -> (u16, u16)> JoyStick<F> {
    pub fn read(&mut self) -> JoyStickValue {
        let (a, b) = (self.raw_read)();

        // Self calibrate, seems to vary too much between runs
        if (a > self.x_max) {
            self.x_max = a;
            hprintln!("{:?}", self).unwrap();
        }
        if (a < self.x_min) {
            self.x_min = a;
            hprintln!("{:?}", self).unwrap();
        }
        if (b > self.y_max) {
            self.y_max = b;
            hprintln!("{:?}", self).unwrap();
        }
        if (b < self.y_min) {
            self.y_min = b;
            hprintln!("{:?}", self).unwrap();
        }

        let a_centered = a - self.x_min;
        let d_x = self.x_max - self.x_min;
        let a_normalized = (a_centered as f32) / (d_x as f32) * 2.0 - 1.0;

        let b_centered = b - self.y_min;
        let d_y = self.y_max - self.y_min;
        let b_normalized = (b_centered as f32) / (d_y as f32) * 2.0 - 1.0;

        return JoyStickValue {
            x: a_normalized,
            y: b_normalized,
        };
    }
}
