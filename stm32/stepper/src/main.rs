#![no_main]
#![no_std]

#[allow(unused)]
use panic_halt;

use crate::hal::delay::Delay;
use crate::hal::prelude::*;

// use crate::hal::pwm::{PwmMode, PwmModeSet};
use stm32f1xx_hal as hal;

use stepper::motor::Motor;
use stepper::pwm_channel_mode::{MiscPWMStuff, PwmMode, PwmModeSet};
use stepper::shift_register::shift_register_send;

//use cortex_m_rt::entry;
use cortex_m_semihosting::hprintln;

use rtfm::app;

macro_rules! setup_debug_pins {
    ($gpioa:ident) => {{
        let a10 = $gpioa
            .pa10
            .into_open_drain_output_with_state(&mut $gpioa.crh, hal::gpio::State::Low);
        a10
    }};
}

macro_rules! setup_shift_register {
    ($gpioa:ident, $gpiob:ident, $delay:ident) => {{
        let mut clock = $gpioa.pa6.into_push_pull_output(&mut $gpioa.crl);
        let mut latch = $gpioa.pa7.into_push_pull_output(&mut $gpioa.crl);
        let mut data = $gpiob.pb11.into_push_pull_output(&mut $gpiob.crh);

        for v in [true, true, false, false, true, true, false, false].iter() {
            // shift_register_send(&mut clock, &mut latch, &mut data, v.clone());
            latch.set_low();
            match v {
                true => data.set_high(),
                false => data.set_low(),
            }

            clock.set_high();
            clock.set_low();

            latch.set_high();
        }

        // Want it to float so that the feedback wire takes over
        data.into_floating_input(&mut $gpiob.crh);
        (clock, latch)
    }};
}

macro_rules! setup_pwm {
    ($device:ident, $gpioa:ident, $gpiob:ident, $a6:ident, $a7:ident, $afio:ident, $rcc:ident, $clocks:ident) => {{
        // Will override arr and psc so this will not matter any
        let meaningless_freq = 1.hz();

        // PWM 1, controls two output pins
        // pa6,7, pb0,1  (Note that pb1 is in use as boot thingy and not of much use
        // we only actually use pa6+7 but pwm trait needs us to supply b0 and b1 too
        let a6 = $a6.into_alternate_push_pull(&mut $gpioa.crl);
        let mut a7 = $a7.into_alternate_push_pull(&mut $gpioa.crl);
        let b0 = $gpiob.pb0.into_alternate_push_pull(&mut $gpiob.crl);
        let b1 = $gpiob.pb1.into_alternate_push_pull(&mut $gpiob.crl);
        let mut pwm_1 = $device.TIM3.pwm(
            (a6, a7, b0, b1),
            &mut $afio.mapr,
            meaningless_freq,
            $clocks,
            &mut $rcc.apb1,
        );
        // We don't want it to start yet
        // Note: This disable the entire pwm_1,  not only pwm_1.0
        unsafe { pwm_1.0.disable_pwm() };
        unsafe {
            pwm_1.0.write_arr(8);
        };

        pwm_1.0.set_duty(pwm_1.0.get_max_duty() / 2);

        // This essentially negates the output channel, search for "Pwm Mode 2" in reference manual
        pwm_1.1.set_mode(PwmMode::Mode2);
        pwm_1.1.set_duty(pwm_1.1.get_max_duty() / 2);

        pwm_1.0.enable();
        pwm_1.1.enable();
        unsafe { pwm_1.0.enable_pwm() };
        pwm_1
    }};
}

#[app(device = stm32f1xx_hal::pac)]
const APP: () = {
    static mut DELAY: Delay = ();
    #[init]
    fn init() -> init::LateResources {
        hprintln!("Initi").unwrap();
        // We have some magic from rtfm:
        // core: rtfm::Peripherals
        // device: stm32f1xx_hal::pac::Peripherals
        // Pretty much same as:
        //        let device = stm32::Peripherals::take().unwrap();
        //        let core = Peripherals::take().unwrap();

        // Constrain clocking registers
        let mut flash = device.FLASH.constrain();
        let mut rcc = device.RCC.constrain();
        let mut afio = device.AFIO.constrain(&mut rcc.apb2);

        let clocks = rcc.cfgr.freeze(&mut flash.acr);
        let mut gpioa = device.GPIOA.split(&mut rcc.apb2);
        let mut gpiob = device.GPIOB.split(&mut rcc.apb2);
        hprintln!("Sysclk: {:?}", clocks.sysclk().0).unwrap();
        // Get delay provider
        let mut delay = Delay::new(core.SYST, clocks);

        // Pin PB10
        let mut debug_pin = setup_debug_pins!(gpioa);

        debug_pin.set_high();
        let (a6, a7) = setup_shift_register!(gpioa, gpiob, delay);

        // Configure pwm
        // PINS:
        // PWM1: PA6, PA7, (B0, B1)
        // PWM2: PB6, PB7, (PB8, PB9)
        let mut pwm = setup_pwm!(device, gpioa, gpiob, a6, a7, afio, rcc, clocks);
        //        debug_pin.set_low();

        unsafe { pwm.0.write_psc(2000) };
        let mut psc = unsafe { pwm.0.read_psc() };
        hprintln!("PSC: {:?}", psc).unwrap();
        // Fast
        let fast_limits: [[u16; 2]; 4] = [
            // Target psc, step size
            [2000, 1], // Just for starting speed
            [1000, 100],
            [500, 50],
            [225, 10],
        ];

        let limits = fast_limits;
        //let limits = fast_limits;

        let mut v = true;
        debug_pin.set_low();
        for &[target, delta] in limits.iter() {
            while psc > target {
                psc -= delta;
                unsafe { pwm.0.write_psc(psc) };
                if v {
                    debug_pin.set_high();
                } else {
                    debug_pin.set_low();
                }
                v = !v;
                delay.delay_ms(50 as u16);
            }
        }

        init::LateResources { DELAY: delay }
    }

    #[idle(resources= [DELAY])]
    fn idle() -> ! {
        loop {}

        // motor.idle();
        // hprintln!("Stopping").unwrap();
    }
};
