use embedded_hal::digital::v2::OutputPin;
use nb;
use nb::block;

pub const STEPS: [[bool; 4]; 4] = [
    [true, false, true, false],
    [false, true, true, false],
    [false, true, false, true],
    [true, false, false, true],
];

pub struct Motor<
    A1: OutputPin + Sized,
    A2: OutputPin + Sized,
    A3: OutputPin + Sized,
    A4: OutputPin + Sized,
> {
    a1: A1,
    a2: A2,
    a3: A3,
    a4: A4,
    position: u8,
    number_of_steps: u8,
}

impl<A1, A2, A3, A4> Motor<A1, A2, A3, A4>
where
    A1: OutputPin + Sized,
    A2: OutputPin + Sized,
    A3: OutputPin + Sized,
    A4: OutputPin + Sized,
{
    pub fn new(a1: A1, a2: A2, a3: A3, a4: A4) -> Motor<A1, A2, A3, A4> {
        Motor {
            a1: a1,
            a2: a2,
            a3: a3,
            a4: a4,
            position: 0,
            number_of_steps: 200,
        }
    }

    pub fn set_position(&mut self, position: u8, clockwise: bool, mut wait: impl FnMut() -> ()) {
        while self.position != position {
            if clockwise {
                self.step();
            } else {
                self.step_reverse();
            }
            wait();
        }
    }

    pub fn step(&mut self) {
        self.position = (self.position + 1) % self.number_of_steps;
        self.set_pins(&STEPS[(self.position % 4) as usize])
    }

    pub fn step_reverse(&mut self) {
        self.position = match self.position {
            0 => self.number_of_steps - 1,
            _ => self.position - 1,
        };
        self.set_pins(&STEPS[(self.position % 4) as usize])
    }

    pub fn step_towards(&mut self, position: u8) {
        if self.position == position {
            self.idle();
            return;
        }
        //
        // [a, c, t, b]
        // path1: c - a  + b - t= c - t + 200
        // path2: t - c
        //
        // [a, t, c, b]
        // path1: c - t
        // path2: b - c + t
        let (d1, d2) = match self.position < position {
            true => (200 - position + self.position, position - self.position),
            false => (self.position - position, 200 - self.position + position),
        };
        if d1 > d2 {
            self.step();
        } else {
            self.step_reverse();
        }
    }

    pub fn idle(&mut self) {
        self.set_pins(&[false, false, false, false]);
    }

    pub fn set_pins(&mut self, values: &[bool; 4]) {
        // This is stupid, what's better way?
        match values[0] {
            true => self.a1.set_high(),
            false => self.a1.set_low(),
        };
        match values[1] {
            true => self.a2.set_high(),
            false => self.a2.set_low(),
        };
        match values[2] {
            true => self.a3.set_high(),
            false => self.a3.set_low(),
        };
        match values[3] {
            true => self.a4.set_high(),
            false => self.a4.set_low(),
        };
    }
}
