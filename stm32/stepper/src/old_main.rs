// Manual gpio
#![no_main]
#![no_std]

#[allow(unused)]
use panic_halt;

use crate::hal::adc;
use crate::hal::delay::Delay;
use crate::hal::prelude::*;
use crate::hal::stm32;
use stm32f1xx_hal as hal;

use stepper::joystick::JoyStick;
use stepper::motor::Motor;

use cortex_m::peripheral::Peripherals;

use cortex_m_rt::entry;
use cortex_m_semihosting::hprintln;

use rtfm::app;

#[app(device = stm32f1xx_hal::pac)]
const APP: () = {
    static mut MOTOR: Motor<
        hal::gpio::gpioa::PA1<hal::gpio::Output<hal::gpio::PushPull>>,
        hal::gpio::gpioa::PA2<hal::gpio::Output<hal::gpio::PushPull>>,
        hal::gpio::gpioa::PA3<hal::gpio::Output<hal::gpio::PushPull>>,
        hal::gpio::gpioa::PA4<hal::gpio::Output<hal::gpio::PushPull>>,
    > = ();

    static mut DELAY: Delay = ();
    static mut ADC: adc::Adc<hal::pac::ADC1> = ();
    static mut CH0: hal::gpio::gpiob::PB0<hal::gpio::Analog> = ();
    static mut CH1: hal::gpio::gpiob::PB1<hal::gpio::Analog> = ();

    #[init]
    fn init() -> init::LateResources {
        hprintln!("Initi").unwrap();
        // We have some magic from rtfm:
        // core: rtfm::Peripherals
        // device: stm32f1xx_hal::pac::Peripherals
        // Pretty much same as:
        //        let device = stm32::Peripherals::take().unwrap();
        //        let core = Peripherals::take().unwrap();

        // Constrain clocking registers
        let mut flash = device.FLASH.constrain();
        let mut rcc = device.RCC.constrain();
        let afio = device.AFIO.constrain(&mut rcc.apb2);

        let clocks = rcc.cfgr.freeze(&mut flash.acr);
        let mut gpioa = device.GPIOA.split(&mut rcc.apb2);
        let mut gpiob = device.GPIOB.split(&mut rcc.apb2);
        hprintln!("Sysclk: {:?}", clocks.sysclk().0).unwrap();
        // Get delay provider
        let delay = Delay::new(core.SYST, clocks);

        // Configure pins for motor
        let a1 = gpioa.pa1.into_push_pull_output(&mut gpioa.crl);
        let a2 = gpioa.pa2.into_push_pull_output(&mut gpioa.crl);
        let a3 = gpioa.pa3.into_push_pull_output(&mut gpioa.crl);
        let a4 = gpioa.pa4.into_push_pull_output(&mut gpioa.crl);

        let motor = Motor::new(a1, a2, a3, a4);

        let mut analog_input_x = gpiob.pb0.into_analog(&mut gpiob.crl);
        let mut analog_input_y = gpiob.pb1.into_analog(&mut gpiob.crl);

        let mut adc = adc::Adc::adc1(device.ADC1, &mut rcc.apb2);

        let _x: u16 = adc.read(&mut analog_input_x).unwrap();

        init::LateResources {
            MOTOR: motor,
            DELAY: delay,
            // SHARED_EXTI: device.EXTI,
            ADC: adc,
            CH0: analog_input_x,
            CH1: analog_input_y,
        }
    }

    //    #[idle(resources= [MOTOR, DELAY, ADC, CH0, CH1])]
    //    fn idle() -> ! {
    //        let motor = &mut resources.MOTOR;
    //        let delay = &mut resources.DELAY;
    //        let adc = resources.ADC;
    //        let analog_input_x = resources.CH0;
    //        let analog_input_y = resources.CH1;
    //
    //        let read = || {
    //            (
    //                adc.read(analog_input_x).unwrap(),
    //                adc.read(analog_input_y).unwrap(),
    //            )
    //        };
    //        let mut joystick = JoyStick {
    //            raw_read: read,
    //            x_min: 0,
    //            y_min: 0,
    //            x_max: 4095,
    //            y_max: 4095,
    //        };
    //
    //        hprintln!("Starting").unwrap();
    //
    //        joystick.read();
    //        loop {
    //            let value = joystick.read();
    //            if value.x * value.x + value.y * value.y  < 0.25 {
    //                motor.idle();
    //            } else {
    //                let angle = value.angle();
    //                let  position = (200.0 * angle / (2.0 * core::f32::consts::PI)) as u8;
    //                // hprintln!("V: {:?} A: {:?} P: {:?}", value, angle, position).unwrap();
    //                motor.step_towards(position % 200 );
    //            }
    //
    //            delay.delay_ms(10 as u8);
    //        }
    //        motor.idle();
    //        hprintln!("Stopping").unwrap();
    //    }

    #[idle(resources= [MOTOR, DELAY, ADC, CH0, CH1])]
    fn idle() -> ! {
        let motor = &mut resources.MOTOR;
        let delay = &mut resources.DELAY;
        let adc = resources.ADC;

        // Fast
        let fast_limits: [[u32; 2]; 5] = [
            // Target speed, Step size
            [8000, 1], // Just for starting speed
            [1000, 500],
            [100, 10],
            [50, 5],
            [2, 1], // Faster than we can actually manage...
        ];

        let slow_limits: [[u32; 2]; 2] = [
            // Target speed, Step size
            [500_000, 1], // Just for starting speed
            [100_000, 10_000],
        ];

        let limits = slow_limits;
        //let limits = fast_limits;

        // Starting speed
        let mut delay_us: u32 = limits[0][0];

        // Steps at final target speed before idle
        let mut extra_steps: u32 = 200 * 200;

        for &[target, delta] in limits.iter() {
            while delay_us > target {
                delay_us -= delta;
                motor.step();
                delay.delay_us(delay_us);
            }
        }

        while extra_steps > 0 {
            extra_steps -= 1;
            motor.step();
            delay.delay_us(delay_us);
        }
        motor.idle();

        loop {

        }

        // motor.idle();
        // hprintln!("Stopping").unwrap();
    }
};
