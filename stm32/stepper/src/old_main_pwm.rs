// PWM, not really working, phase shift between timers not correct
#![no_main]
#![no_std]

#[allow(unused)]
use panic_halt;

use crate::hal::delay::Delay;
use crate::hal::prelude::*;

// use crate::hal::pwm::{PwmMode, PwmModeSet};
use stm32f1xx_hal as hal;

use stepper::motor::Motor;
use stepper::pwm_channel_mode::{MiscPWMStuff, PwmMode, PwmModeSet};

//use cortex_m_rt::entry;
use cortex_m_semihosting::hprintln;

use rtfm::app;

macro_rules! setup_debug_pins {
    ($gpioa:ident) => {{
        let a10 = $gpioa.pa10.into_open_drain_output_with_state(&mut $gpioa.crh, hal::gpio::State::Low);
        a10
    }}
}


macro_rules! setup_motor {
    ($gpioa:ident) => {{
        // Set up motor
        let a1 = ($gpioa).pa1.into_push_pull_output(&mut $gpioa.crl);
        let a2 = $gpioa.pa2.into_push_pull_output(&mut $gpioa.crl);
        let a3 = $gpioa.pa3.into_push_pull_output(&mut $gpioa.crl);
        let a4 = $gpioa.pa4.into_push_pull_output(&mut $gpioa.crl);

        Motor::new(a1, a2, a3, a4)
    }};
}

macro_rules! setup_pwm {
    ($device:ident, $gpioa:ident, $gpiob:ident, $afio:ident, $rcc:ident, $clocks:ident) => {
        let pwm_freq = 800.hz();

        // PWM 1, controls two output pins
        // pa6,7, pb0,1  (Note that pb1 is in use as boot thingy and not of much use
        // we only actually use pa6+7 but pwm trait needs us to supply b0 and b1 too
        let a6 = $gpioa.pa6.into_alternate_push_pull(&mut $gpioa.crl);
        let mut a7 = $gpioa.pa7.into_alternate_push_pull(&mut $gpioa.crl);
        let b0 = $gpiob.pb0.into_alternate_push_pull(&mut $gpiob.crl);
        let b1 = $gpiob.pb1.into_alternate_push_pull(&mut $gpiob.crl);
        let mut pwm_1 = $device.TIM3.pwm(
            (a6, a7, b0, b1),
            &mut $afio.mapr,
            pwm_freq,
            $clocks,
            &mut $rcc.apb1,
        );
        // We don't want it to start yet
        // Note: This disable the entire pwm_1,  not only pwm_1.0
        unsafe { pwm_1.0.disable_pwm() };

        pwm_1.0.set_duty(pwm_1.0.get_max_duty() / 2);

        // This essentially negates the output channel, search for "Pwm Mode 2" in reference manual
        pwm_1.1.set_mode(PwmMode::Mode2);
        pwm_1.1.set_duty(pwm_1.1.get_max_duty() / 2);

        let b6 = $gpiob.pb6.into_alternate_push_pull(&mut $gpiob.crl);
        let b7 = $gpiob.pb7.into_alternate_push_pull(&mut $gpiob.crl);
        let b8 = $gpiob.pb8.into_alternate_push_pull(&mut $gpiob.crh);
        let b9 = $gpiob.pb9.into_alternate_push_pull(&mut $gpiob.crh);

        let mut pwm_2 = $device.TIM4.pwm(
            (b6, b7, b8, b9),
            &mut $afio.mapr,
            pwm_freq,
            $clocks,
            &mut $rcc.apb1,
        );
        // We don't want it to start yet
        // Note: This disable the entire pwm_2,  not only pwm_2.0
        unsafe { pwm_2.0.disable_pwm() };

        pwm_2.0.set_duty(pwm_2.0.get_max_duty() / 2);

        // This essentially negates the output channel, search for "Pwm Mode 2" in reference manual
        pwm_2.1.set_mode(PwmMode::Mode2);
        pwm_2.1.set_duty(pwm_2.1.get_max_duty() / 2);

        unsafe {
            pwm_1.0.set_master();
            pwm_2.0.set_slave();

            pwm_1.0.set_counter(0);
            let max_duty = pwm_2.0.get_max_duty();
            hprintln!("Max duty: {:?}", max_duty).unwrap();
            pwm_2.0.set_counter(pwm_2.0.get_max_duty() / 2);

            let cnt_1 = pwm_1.0.get_counter();
            let cnt_2 = pwm_2.0.get_counter();
            hprintln!("CNT: {:?}  {:?}", cnt_1, cnt_2).unwrap();

            // Enable the actual timer again
            pwm_1.0.enable_pwm();

            // Enable the outputs
            pwm_1.0.enable();
            pwm_1.1.enable();
            pwm_2.0.enable();
            pwm_2.1.enable();

            let cnt_1 = pwm_1.0.get_counter();
            let cnt_2 = pwm_2.0.get_counter();
            hprintln!("CNT: {:?}  {:?}", cnt_1, cnt_2).unwrap();

            let cnt_1 = pwm_1.0.get_counter();
            let cnt_2 = pwm_2.0.get_counter();
            hprintln!("CNT: {:?}  {:?}", cnt_1, cnt_2).unwrap();

            let cnt_1 = pwm_1.0.get_counter();
            let cnt_2 = pwm_2.0.get_counter();
            hprintln!("CNT: {:?}  {:?}", cnt_1, cnt_2).unwrap();
        }

        // delay.delay_ms(2000 as u32);

        // Disables entire pwm_2, not just one channel
        unsafe {
            pwm_2.0.disable_pwm();
        }
    };
}

#[app(device = stm32f1xx_hal::pac)]
const APP: () = {
    static mut MOTOR: Motor<
        hal::gpio::gpioa::PA1<hal::gpio::Output<hal::gpio::PushPull>>,
        hal::gpio::gpioa::PA2<hal::gpio::Output<hal::gpio::PushPull>>,
        hal::gpio::gpioa::PA3<hal::gpio::Output<hal::gpio::PushPull>>,
        hal::gpio::gpioa::PA4<hal::gpio::Output<hal::gpio::PushPull>>,
    > = ();

    static mut DELAY: Delay = ();
    #[init]
    fn init() -> init::LateResources {
        hprintln!("Initi").unwrap();
        // We have some magic from rtfm:
        // core: rtfm::Peripherals
        // device: stm32f1xx_hal::pac::Peripherals
        // Pretty much same as:
        //        let device = stm32::Peripherals::take().unwrap();
        //        let core = Peripherals::take().unwrap();

        // Constrain clocking registers
        let mut flash = device.FLASH.constrain();
        let mut rcc = device.RCC.constrain();
        let mut afio = device.AFIO.constrain(&mut rcc.apb2);

        let clocks = rcc.cfgr.freeze(&mut flash.acr);
        let mut gpioa = device.GPIOA.split(&mut rcc.apb2);
        let mut gpiob = device.GPIOB.split(&mut rcc.apb2);
        hprintln!("Sysclk: {:?}", clocks.sysclk().0).unwrap();
        // Get delay provider
        let mut delay = Delay::new(core.SYST, clocks);

        // Pin PB10
        let mut debug_pin = setup_debug_pins!(gpioa);

        // Configure pins for motor chip, cpu control
        // Pin a1-a4, motor
        let motor = setup_motor!(gpioa);
        debug_pin.set_high();

        // Configure pwm
        // PINS:
        // PWM1: PA6, PA7, (B0, B1)
        // PWM2: PB6, PB7, (PB8, PB9)
        setup_pwm!(device, gpioa, gpiob, afio, rcc, clocks);
        debug_pin.set_low();

        init::LateResources {
            MOTOR: motor,
            DELAY: delay,
        }
    }

    #[idle(resources= [MOTOR, DELAY])]
    fn idle() -> ! {
        let motor = &mut resources.MOTOR;
        let delay = &mut resources.DELAY;
        // Fast
        let fast_limits: [[u32; 2]; 5] = [
            // Target speed, Step size
            [8000, 1], // Just for starting speed
            [1000, 500],
            [100, 10],
            [50, 5],
            [2, 1], // Faster than we can actually manage...
        ];

        let slow_limits: [[u32; 2]; 2] = [
            // Target speed, Step size
            [500_000, 1], // Just for starting speed
            [250_000, 10_000],
        ];

        // let limits = slow_limits;
        let limits = fast_limits;

        // Starting speed
        let mut delay_us: u32 = limits[0][0];

        // Steps at final target speed before idle
        let mut extra_steps: u32 = 200 * 2000;

        for &[target, delta] in limits.iter() {
            while delay_us > target {
                delay_us -= delta;
                motor.step();
                delay.delay_us(delay_us);
            }
        }

        while extra_steps > 0 {
            extra_steps -= 1;
            motor.step();
            delay.delay_us(delay_us);
        }
        motor.idle();

        loop {}

        // motor.idle();
        // hprintln!("Stopping").unwrap();
    }
};
