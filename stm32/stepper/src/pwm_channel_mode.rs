use crate::hal::pac::{TIM2, TIM3, TIM4};
use crate::hal::pwm::{Pwm, C1, C2, C3, C4};

pub enum PwmMode {
    Mode1,
    Mode2,
}

impl PwmMode {
    fn value(&self) -> u8 {
        match self {
            PwmMode::Mode1 => 6,
            PwmMode::Mode2 => 7,
        }
    }
}

pub trait PwmModeSet {
    fn set_mode(&mut self, mode: PwmMode);
}

pub trait MiscPWMStuff {
    unsafe fn disable_pwm(&mut self);
    unsafe fn enable_pwm(&mut self);
    unsafe fn set_master(&mut self);
    unsafe fn set_slave(&mut self);
    unsafe fn set_counter(&mut self, value: u16);
    unsafe fn get_counter(&self) -> u16;
    unsafe fn write_psc(&mut self, psc: u16);
    unsafe fn read_psc(&mut self) -> u16;
    unsafe fn write_arr(&mut self, arr: u16);
    unsafe fn read_arr(&mut self) -> u32;
}

macro_rules! hal {
    ($($TIMX:ident: (),)+) => {
        $(

            // NOTE! This is quite stupid escape-hatchy stuff to bypass the hal
            impl MiscPWMStuff for Pwm<$TIMX, C1> {
                // Will disable/enable for *All* channels
                unsafe fn disable_pwm(&mut self) {
                    unsafe {
                        &(*$TIMX::ptr()).cr1.write(|w| unsafe {
                            w.cen()
                                .clear_bit()
                        });
                    }
                }
                unsafe fn enable_pwm(&mut self) {
                    unsafe {
                        &(*$TIMX::ptr()).cr1.write(|w| unsafe {
                            w.cen()
                                .set_bit()
                        });
                    }
                }
                unsafe fn set_master(&mut self) {
                    unsafe {
                        &(*$TIMX::ptr()).cr2.write(|w|
                            w.mms().update()
                        )
                    };
                }
                unsafe fn set_slave(&mut self) {
                    // Set as slave controlled by TIM3
                    &(*$TIMX::ptr()).smcr.write(|w| unsafe {
                        w.ts()
                            .itr2()
                            .sms()
                            .trigger_mode()
                    });
                }
                unsafe fn set_counter(&mut self, value: u16) {
                    &(*$TIMX::ptr()).cnt.write(|w| unsafe {
                        w.cnt().bits(value)
                    });
                }
                unsafe fn get_counter(&self) -> u16 {
                    let v = &(*$TIMX::ptr()).cnt.read().cnt().bits();
                    return v.clone()
                }

                unsafe fn write_psc(&mut self, psc: u16) {
                    &(*$TIMX::ptr()).psc.write(|w| unsafe { w.psc().bits(psc) });
                }
                unsafe fn read_psc(&mut self) -> u16 {
                    let bits = &(*$TIMX::ptr()).psc.read().psc().bits();
                    return bits.clone();
                }

                unsafe fn write_arr(&mut self, arr: u16) {
                    &(*$TIMX::ptr()).arr.write(|w| { w.arr().bits(arr) });
                }
                unsafe fn read_arr(&mut self) -> u32 {
                    let bits = &(*$TIMX::ptr()).arr.read().bits();
                    return bits.clone();
                }
//                unsafe fn set_slave(&mut self)
            }
            impl PwmModeSet for Pwm<$TIMX, C1> {
                fn set_mode(&mut self, mode: PwmMode) {
                    unsafe {
                       &(*$TIMX::ptr()).ccmr1_output
                        .modify(|_, w| unsafe { w.oc1m().bits(mode.value()) });
                    }
                }
            }
            impl PwmModeSet for Pwm<$TIMX, C2> {
                fn set_mode(&mut self, mode: PwmMode) {
                    unsafe {
                       &(*$TIMX::ptr()).ccmr1_output
                        .modify(|_, w| unsafe { w.oc2m().bits(mode.value()) });
                    }
                }
            }


            // The the other ones I don't use, and would require some more care
//                        impl PwmModeSet for Pwm<$TIMX, C3> {
//                fn set_mode(&mut self, mode: PwmMode) {
//                    unsafe {
//                       &(*$TIMX::ptr()).ccmr1_output
//                        .modify(|_, w| unsafe { w.oc3m().bits(mode.value()) });
//                    }
//                }
//            }
//                    impl PwmModeSet for Pwm<$TIMX, C4> {
//                fn set_mode(&mut self, mode: PwmMode) {
//                    unsafe {
//                       &(*$TIMX::ptr()).ccmr1_output
//                        .modify(|_, w| unsafe { w.oc4m().bits(mode.value()) });
//                    }
//                }
//            }
        )+
    }
}

hal! {
    //TIM2: (oc2m),
    TIM3: (),
    TIM4: (),
}
