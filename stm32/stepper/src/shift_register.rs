use embedded_hal::digital::OutputPin;

pub fn shift_register_send(
    clock: &mut OutputPin,
    latch: &mut OutputPin,
    data: &mut OutputPin,
    v: bool,
) {
    latch.set_low();

    match v {
        true => data.set_high(),
        false => data.set_low(),
    }

    clock.set_high();
    clock.set_low();

    latch.set_high();
}
