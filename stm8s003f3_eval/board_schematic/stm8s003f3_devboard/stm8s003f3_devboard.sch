EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_ST_STM8:STM8S003F3P U1
U 1 1 5E2D939A
P 4200 3700
F 0 "U1" H 4200 4881 50  0000 C CNN
F 1 "STM8S003F3P" H 4200 4790 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 4250 4800 50  0001 L CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00024550.pdf" H 4150 3300 50  0001 C CNN
	1    4200 3700
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR0101
U 1 1 5E2D9FE7
P 4200 2700
F 0 "#PWR0101" H 4200 2550 50  0001 C CNN
F 1 "VDD" H 4217 2873 50  0000 C CNN
F 2 "" H 4200 2700 50  0001 C CNN
F 3 "" H 4200 2700 50  0001 C CNN
	1    4200 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR0102
U 1 1 5E2DA519
P 4200 4700
F 0 "#PWR0102" H 4200 4450 50  0001 C CNN
F 1 "GNDREF" H 4205 4527 50  0000 C CNN
F 2 "" H 4200 4700 50  0001 C CNN
F 3 "" H 4200 4700 50  0001 C CNN
	1    4200 4700
	1    0    0    -1  
$EndComp
Text Label 3600 3000 2    50   ~ 0
OSC_A
$Comp
L Device:C C5
U 1 1 5E2EA8C6
P 3450 4550
F 0 "C5" H 3565 4596 50  0000 L CNN
F 1 "10nF" H 3565 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3488 4400 50  0001 C CNN
F 3 "~" H 3450 4550 50  0001 C CNN
	1    3450 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 4400 3450 4400
Wire Wire Line
	3450 4700 4200 4700
Connection ~ 4200 4700
$Comp
L Connector:Conn_01x17_Male J1
U 1 1 5E2EC728
P 4900 6300
F 0 "J1" V 4827 6278 50  0000 C CNN
F 1 "Conn_01x17_Male" V 4736 6278 50  0000 C CNN
F 2 "custom:Connection_PinsSmd_17" H 4900 6300 50  0001 C CNN
F 3 "~" H 4900 6300 50  0001 C CNN
	1    4900 6300
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR0104
U 1 1 5E2F1D18
P 5100 5500
F 0 "#PWR0104" H 5100 5350 50  0001 C CNN
F 1 "VDD" V 5117 5628 50  0000 L CNN
F 2 "" H 5100 5500 50  0001 C CNN
F 3 "" H 5100 5500 50  0001 C CNN
	1    5100 5500
	0    1    1    0   
$EndComp
$Comp
L power:GNDREF #PWR0105
U 1 1 5E2F89FF
P 5100 5600
F 0 "#PWR0105" H 5100 5350 50  0001 C CNN
F 1 "GNDREF" V 5105 5472 50  0000 R CNN
F 2 "" H 5100 5600 50  0001 C CNN
F 3 "" H 5100 5600 50  0001 C CNN
	1    5100 5600
	0    -1   -1   0   
$EndComp
Text Label 5100 5800 0    50   ~ 0
NRST
Text Label 5100 5900 0    50   ~ 0
PD6
Text Label 5100 6000 0    50   ~ 0
PD5
Text Label 5100 6100 0    50   ~ 0
PD4
Text Label 5100 6200 0    50   ~ 0
PD3
Text Label 5100 6300 0    50   ~ 0
PD2
Text Label 5100 6400 0    50   ~ 0
PD1
Text Label 5100 6500 0    50   ~ 0
PC7
Text Label 5100 6600 0    50   ~ 0
PC6
Text Label 5100 6700 0    50   ~ 0
PC5
Text Label 5100 6800 0    50   ~ 0
PC4
Text Label 5100 6900 0    50   ~ 0
PC3
Text Label 5100 7000 0    50   ~ 0
PB4
Text Label 5100 7100 0    50   ~ 0
PB5
Text Label 4800 3400 0    50   ~ 0
PD1
Text Label 4800 3500 0    50   ~ 0
PD2
Text Label 4800 3600 0    50   ~ 0
PD3
Text Label 4800 3700 0    50   ~ 0
PD4
Text Label 4800 3800 0    50   ~ 0
PD5
Text Label 4800 3900 0    50   ~ 0
PD6
Text Label 3600 3700 0    50   ~ 0
PC3
Text Label 3600 3800 0    50   ~ 0
PC4
Text Label 3600 3900 0    50   ~ 0
PC5
Text Label 3600 4000 0    50   ~ 0
PC6
Text Label 3600 4100 0    50   ~ 0
PC7
Text Label 3600 3400 2    50   ~ 0
PB4
Text Label 3600 3500 2    50   ~ 0
PB5
Text Label 3600 4300 2    50   ~ 0
NRST
$Comp
L Oscillator:SG-5032CAN X1
U 1 1 5E31019E
P 1900 4500
F 0 "X1" H 2244 4546 50  0000 L CNN
F 1 "SG-5032CAN" H 2244 4455 50  0000 L CNN
F 2 "Oscillator:Oscillator_SMD_SeikoEpson_SG8002LB-4Pin_5.0x3.2mm" H 2600 4150 50  0001 C CNN
F 3 "https://support.epson.biz/td/api/doc_check.php?dl=brief_SG5032CAN&lang=en" H 1800 4500 50  0001 C CNN
	1    1900 4500
	1    0    0    -1  
$EndComp
Text Label 2200 4500 0    50   ~ 0
OSC_A
$Comp
L power:VDD #PWR0103
U 1 1 5E3126C5
P 1900 4200
F 0 "#PWR0103" H 1900 4050 50  0001 C CNN
F 1 "VDD" H 1917 4373 50  0000 C CNN
F 2 "" H 1900 4200 50  0001 C CNN
F 3 "" H 1900 4200 50  0001 C CNN
	1    1900 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR0106
U 1 1 5E312C0F
P 1900 4800
F 0 "#PWR0106" H 1900 4550 50  0001 C CNN
F 1 "GNDREF" H 1905 4627 50  0000 C CNN
F 2 "" H 1900 4800 50  0001 C CNN
F 3 "" H 1900 4800 50  0001 C CNN
	1    1900 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 4500 1600 4200
Wire Wire Line
	1600 4200 1900 4200
Connection ~ 1900 4200
NoConn ~ 3600 3100
NoConn ~ 3600 3200
NoConn ~ 5100 5700
$EndSCHEMATC
