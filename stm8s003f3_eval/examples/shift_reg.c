/*
SCK: PC5
MOSI: PC6
LATCH: PC7
*/
#include <stdint.h>
#include "stm8s.h"
#include "utils.h"

#define PIN_STATUS 4

void setup_spi() {

    // Make SCK and MOSI output pins
    // PC_DDR |= (1 << 5) | (1 << 6);
    // PC_CR1 |= (1 << 5) | (1 << 6);
    // PC_CR2 |= (1 << 5) | (1 << 6);

    // Make NSS an output pin
    PC_DDR |= (1 << 7);
    PC_CR1 |= (1 << 7);
    PC_CR2 |= (1 << 7);

    // Reset state
    SPI_CR2 = 0;
    SPI_CR1 = 0;

    // Software slave management (no hardware NSS pin)
    //SPI_CR2 |= SPI_CR2_SSM;

    // Shift register can easily keep up at 5V
    SPI_CR1 |= SPI_CR1_BRR_DIV2;

    // Enable
    SPI_CR1 |= SPI_CR1_MSTR | SPI_CR1_SPE;

}

void setup_status_led()
{
    // Set pin as output push-pull
    PC_DDR |= (1 << PIN_STATUS);
    PC_CR1 |= (1 << PIN_STATUS);

    // Make it switch at 10MHz because why not
    PC_CR2 |= (1 << PIN_STATUS);
}

inline void status_led_on()
{
    PC_ODR |= (1 << PIN_STATUS);
}

inline void status_led_off()
{
    PC_ODR &= ~(1 << PIN_STATUS);
}

inline void toggle_nss()
{
    PC_ODR &= ~(1 << 7);
    PC_ODR |= (1 << 7);
}

void write_spi(uint8_t byte) {
    SPI_DR = byte;
    while (!(SPI_SR & SPI_SR_TXE)) {
        // Wait for TXE
    }
    toggle_nss();
}

void write_many() {
    for (uint8_t i = 0; i < 32; i++) {
        SPI_DR = i;
        while (!(SPI_SR & SPI_SR_TXE));
    }
    toggle_nss();
}

void setup_clock()
{
    CLK_CKDIVR = 0;

    // Use the external clock
    CLK_SWCR = CLK_SWCR_SWEN;
    CLK_SWR = CLK_SWR_SWI_HSE;

    // Wait for the clock to become ready
    while (CLK_SWCR & CLK_SWCR_SWBSY)
    {
    }
}

int main() {
    setup_clock();
    setup_spi();
    setup_status_led();

    uint32_t master_clock;
    uint32_t cpu_clock;
    get_clocks(&master_clock, &cpu_clock);

    while (1) {
        status_led_off();
        write_many();
        status_led_on();
        delay_ms(master_clock, 500);
    }
}