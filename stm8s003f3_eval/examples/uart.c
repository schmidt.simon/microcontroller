/*
UART1: TX: PD5  RX: PD6
External interrupt: PC7
Status LED: PC4
*/
#include <stdint.h>
#include "stm8s.h"
#include "utils.h"

#define PIN_EXT 7
#define PIN_LED 4

volatile uint8_t prevent_sleep = 0;

void setup_transmitter(const uint32_t baud_rate)
{
    // Clear M bit for 8 data bits (this is also the default)
    USART1_CR1 &= ~USART_CR1_M;
    USART1_CR3 |= USART_CR3_STOP1;

    // Set baud rate
    // NOTE:
    // * default clock is the HSI, 16MHz
    // * (not important: default clock CPUDIV is 0)
    // * default clock HSIDIV is /8
    // This gives default fMASTER = 2MHz
    // If we want 9600 baud we can get pretty close with 0xd0 divider
    // which gives us 9615 bps
    //
    // also keep in mind the weird structure of the BRR registers
    // BRR2 bits 0-3 are the 4 LSBs of the divider
    // BRR2 bits 4-7 are the 4 MSBs of the divider
    // BRR1 are bits 4-11 of the divider
    uint32_t master_clock;
    uint32_t cpu_clock;
    get_clocks(&master_clock, &cpu_clock);
    const uint16_t divider = master_clock / baud_rate;

    USART1_BRR2 = (divider & 0b111) | ((divider >> 12) << 4);
    USART1_BRR1 |= (divider >> 4) & 0xffff;

    // Set TEN
    USART1_CR2 |= USART_CR2_TEN;
}

void setup_receiver()
{
    // Enable reception and interrupt
    USART1_CR2 |= USART_CR2_REN | USART_CR2_RIEN;
}

void setup_clock()
{

    CLK_CKDIVR &= ~0b111;
    CLK_CKDIVR |= CLK_CKDIVR_CPUDIV8;

    // Use the external clock
    CLK_SWCR = CLK_SWCR_SWEN;
    CLK_SWR = CLK_SWR_SWI_HSE;

    // Wait for the clock to become ready
    while (CLK_SWCR & CLK_SWCR_SWBSY)
    {
    }
}

// The echo buffers
// The input/output buffers are swapped in the main loop
// and then the output buffer is printed.
// When the receiver exceeds the buffer echo_overflow is set
#define ECHO_BUF_LEN 30
char echo_buf_a[ECHO_BUF_LEN];
char echo_buf_b[ECHO_BUF_LEN];

volatile char *echo_buf_input = echo_buf_a;
volatile char *echo_buf_output = echo_buf_b;

volatile uint16_t echo_buf_ix = 0;
volatile uint8_t echo_overflow = 0;

inline uint16_t echo_buffers_swap()
{
    // Swap input/output buffer
    // Returns number of readable bytes in output buffer
    // Disable interrupts to avoid any nasty surprises
    __asm__("sim\n");
    const uint16_t ix = echo_buf_ix;
    echo_buf_ix = 0;

    char * const tmp = echo_buf_input;
    echo_buf_input = echo_buf_output;
    echo_buf_output = tmp;
    __asm__("rim\n");
    return ix;
}

void IRQ_USART1_receive_register_DATA_FULL(void) __interrupt(IRQ_UART1_R_DATA_FULL)
{
    if (USART1_SR & USART_SR_RXNE)
    {
        uint8_t data = USART1_DR;

        const uint16_t ix = echo_buf_ix;
        prevent_sleep = 1;

        if (ix < ECHO_BUF_LEN)
        {
            echo_buf_input[ix] = data;
            echo_buf_ix++;
        }
        else
        {
            echo_overflow = 1;
        }
    }
}

volatile uint8_t got_external_event = 0;
void IRQ_EXTI2_PC_interrupt(void) __interrupt(IRQ_EXTI2)
{
    got_external_event = 1;
    prevent_sleep = 1;
}

void setup_external_pin()
{
    // Make sure it's set as input
    PC_DDR &= ~(1 << PIN_EXT);

    // Input pull-up
    PC_CR1 |= (1 << PIN_EXT);

    // Set EXTI to happen on falling edge
    EXTI_CR1 = EXTI_CR1_FALLING << EXTI_CR1_PCIS;

    // Enable external interrupts
    PC_CR2 |= (1 << PIN_EXT);
}

void setup_status_led()
{
    // Set pin as output push-pull
    PC_DDR |= (1 << PIN_LED);
    PC_CR1 |= (1 << PIN_LED);

    // Make it switch at 10MHz because why not
    PC_CR2 |= (1 << PIN_LED);
}

inline void status_led_on()
{
    PC_ODR |= (1 << PIN_LED);
}

inline void status_led_off()
{
    PC_ODR &= ~(1 << PIN_LED);
}

int main()
{
    setup_clock();

    // Disable interrupts
    __asm__("sim\n");
    setup_receiver();
    setup_transmitter(57600);

    setup_external_pin();
    setup_status_led();

    // Enable interrupts
    __asm__("rim\n");

    print("\nhello\n", 7);

    uint32_t master_clock;
    uint32_t cpu_clock;
    get_clocks(&master_clock, &cpu_clock);

    const char master_msg[] = "master: ";
    const char cpu_msg[] = "cpu: ";

    print(master_msg, 8);
    print_number(master_clock);
    print_char('\n');

    print(cpu_msg, 5);
    print_number(cpu_clock);
    print_char('\n');

    __asm__("halt\n");
    while (1)
    {
        status_led_on();
        prevent_sleep = 0;

        const uint16_t n_output = echo_buffers_swap();
        if (n_output)
        {
            print(echo_buf_output, n_output);
        }

        if (echo_overflow)
        {
            echo_overflow = 0;
            print("\n<overflow>\n", 12);
        }

        if (got_external_event)
        {
            got_external_event = 0;
            print("EXTI2\n", 6);
        }

        // Wait for interrupt, unless some interrupt has happened
        // while we were processing
        status_led_off();
        __asm__("sim\n");

        if (prevent_sleep)
        {
            __asm__("rim\n");
        }
        else
        {
            __asm__("wfi\n");
        }
    }
}