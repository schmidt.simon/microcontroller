#include <stdint.h>
#include "stm8s.h"

#define HSE_CLOCK 16000000;

inline void print_char(const char c)
{

    // This seems redundant and works without
    while (!(USART1_SR & USART_SR_TXE))
    {
        // Wait until clear to transmit
    }

    USART1_DR = c;

    // This seems redundant according to manual
    // but it doesn't work without it
    while (!(USART1_SR & USART_SR_TC))
    {
        // Wait until sent
    }
}

void print(const char message[], const uint32_t length)
{
    for (uint32_t i = 0; i < length; i++)
    {
        print_char(message[i]);
    }
}

void print_number(const uint32_t number)
{
    uint32_t position = 100000000;
    uint8_t first = 0;
    while (position > 0)
    {
        const uint32_t digit = (number / position) % 10;
        position /= 10;
        if (first == 0 && digit == 0)
        {
            continue;
        }
        first = 1;

        print_char((char)(digit + 0x30));
    }
}

/**
 * Calculate currently configured master and cpu clock speed
 */
int get_clocks(uint32_t *master_clock, uint32_t *cpu_clock)
{
    const uint8_t hsi_prescaler = (CLK_CKDIVR >> 3) & 0b11;
    const uint8_t cpu_prescaler = (CLK_CKDIVR & 0b111);
    switch (CLK_CMSR)
    {
    case CLK_CMSR_CKM_HSI:
        *master_clock = 16000000 >> hsi_prescaler;
        break;
    case CLK_CMSR_CKM_LSI:
        *master_clock = 128000;
        break;
    case CLK_CMSR_CKM_HSE:
        *master_clock = HSE_CLOCK;
        break;
    default:
        return -1;
    }

    *cpu_clock = (*master_clock) >> cpu_prescaler;
    return 0;
}


// Roughly on https://gist.github.com/stecman/eed14f9a01d50b9e376429959f35493a
void delay_ms(uint32_t master_clock, uint16_t milliseconds) {
    TIM4_PSCR = 0; // Set prescaler

    uint32_t div = 300;
    uint32_t multiplier = 1;
    while (div > 255) {
        div = (master_clock / (1000 * multiplier));
        if (div > 255) {
            multiplier *= 2;
        }
    }

    TIM4_ARR = (uint8_t)div;

    TIM4_CR1 = TIM_CR1_CEN; // Enable counter

    milliseconds *= multiplier;
    for (; milliseconds > 1; --milliseconds) {
        while ((TIM4_SR & TIM_SR1_UIF) == 0);

        // Clear overflow flag
        TIM4_SR &= ~TIM_SR1_UIF;
    }
}