import machine
import network
import onewire
import ds18x20
import ssd1306
import binascii
import time
import usocket as socket
import json
import gc
import urequests
import ntptime

raise Exception("Put in wifi credentials in configuration")

CONFIG = {
    "DATA_PIN": 16,
    "WIFI_NAME": b"TODO",
    "WIFI_PASSWORD": "TODO",
    "WEBSERVER_ENABLED": False,
    # Leave out to disable
    "OPENTSDB": {
        "url": "http://192.168.1.172:4242",
        "tags": {"test": "temperature"},
        "metric": "thermometer",
    },
}


i2c = machine.I2C(-1, machine.Pin(4), machine.Pin(5), freq=400000)  # Bitbanged I2C bus
oled = ssd1306.SSD1306_I2C(128, 64, i2c)
oled.invert(0)  # White text on black background
oled.contrast(0)  # Minimum contrast (max is 255)
oled.fill(0)

data_pin = machine.Pin(CONFIG["DATA_PIN"])
master = ds18x20.DS18X20(onewire.OneWire(data_pin))


## Global variables
# Assume there is only one sensor
device_ids = master.scan()
print(
    "Found sensors, will use first:",
    [binascii.hexlify(device_id) for device_id in device_ids],
)
device_id = device_ids[0] if device_ids else None
temperature = 0
# Only used for display
ip_address = "Waiting for wifi..."
internet_active = False


update_count = 0
update_error_count = 0
temperature_error = True


def update_temperature():
    # Prior to reading temperature we must issue CONVERT T command
    # This tells the devices to do the A/D and store the result
    # for later reading.
    # Not sure why this doesn't need addressing

    global temperature
    global temperature_error
    global update_count
    global update_error_count

    update_count += 1

    try:
        if temperature_error:
            # Error might be caused by sensor reboot, wait until it's back
            temperature = wait_for_temperature()
        else:
            master.convert_temp()
            temperature = master.read_temp(device_id)
    except Exception:
        update_error_count += 1
        temperature_error = True
    else:
        temperature_error = False


def wait_for_temperature():
    """
    The sensor will report 85 on startup for a while,
    wait until it is done. (wtf, why does it not just error?)
    """
    while True:
        master.convert_temp()
        temperature = master.read_temp(device_id)
        if temperature != 85:
            return temperature
        else:
            time.sleep_ms(750)


def current_context():
    mem_free = gc.mem_free()
    mem_alloc = gc.mem_alloc()
    return dict(
        temperature=temperature,
        device_id=binascii.hexlify(device_id).decode("utf8"),
        memory=100 * mem_alloc / (mem_free + mem_alloc),
        total_errors=update_error_count,
        total_samples=update_count,
        error_rate=100 * update_error_count / update_count,
        ip_address=ip_address,
        error=temperature_error,
    )


DISPLAY_TEXT = """
{ip_address}
Id: {device_id:.8s}...
Temp: {temperature:>4.1f}C
Memory: {memory:>4.1f}%
Errors: {error_rate:>4.1f}%
Error: {error}
""".strip()


def update_screen(context):
    show_text(DISPLAY_TEXT.format(**context))


def show_text(message):
    oled.fill(0)
    for i, row in enumerate(message.split("\n")):
        oled.text(row, 0, i * 10)
    oled.show()


def opentsdb_data_point(submetric, value, timestamp):
    config = CONFIG["OPENTSDB"]
    return {
        "metric": "%s.%s" % (config["metric"], submetric),
        "timestamp": timestamp,
        "value": value,
        "tags": config["tags"],
    }


def update_opentsdb(context):
    if "OPENTSDB" not in CONFIG:
        return

    if not internet_active:
        return

    # micropython uses 2000-01-01 as epoch
    UNIX_EPOCH_OFFSET = 946684800
    timestamp = time.time() + UNIX_EPOCH_OFFSET

    data_points = [
        opentsdb_data_point("error", context["total_errors"], timestamp),
        opentsdb_data_point("memory", context["memory"], timestamp),
    ]
    if not temperature_error:
        data_points.append(
            opentsdb_data_point("value", context["temperature"], timestamp)
        )

    response = urequests.post(
        "%s/api/put" % CONFIG["OPENTSDB"]["url"], json=data_points
    )
    error = False
    if not 200 <= response.status_code < 300:
        error = (response.status_code, response.text)

    response.close()
    if error:
        print(error)
        raise Exception("Unable to update opentsdb")


is_updating = False


def update():
    # Avoid potential racy updates with timers, not sure if possible
    global is_updating
    if is_updating:
        return
    is_updating = True

    try:
        update_temperature()
        context = current_context()
        update_screen(context)

        # TODO slower updates
        update_opentsdb(context)
    finally:
        is_updating = False


def connect_wifi():
    global ip_address
    global internet_active

    print("Connecting to wifi")
    sta_if = network.WLAN(network.STA_IF)
    sta_if.active(True)
    sta_if.connect(CONFIG["WIFI_NAME"], CONFIG["WIFI_PASSWORD"])

    for _ in range(5):
        if sta_if.isconnected():
            break
        time.sleep(1)

    if not sta_if.isconnected():
        ip_address = "Wifi disabled"
        sta_if.active(False)
        raise Exception("Unable to connect to wifi")

    ip_address = "Syncing clock..."
    ntptime.settime()
    ip_address = sta_if.ifconfig()[0]
    internet_active = True


RESPONSE = """
HTTP/1.1 {status}
Content-Type: application/json
Connection: Close

{payload}
""".strip()


def run_webserver():
    http_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    http_socket.bind(("", 80))
    http_socket.listen(3)
    while True:
        client_socket, address = http_socket.accept()
        print("Received connection from", address)

        data = client_socket.recv(1024)
        response = RESPONSE.format(
            payload=json.dumps(current_context()),
            status="500 Sensor Error" if temperature_error else "200 OK",
        )
        client_socket.sendall(response.encode("utf8"))
        client_socket.close()


def main():
    if device_id is None:
        show_text("No sensor")
        return

    show_text("Waiting for sensor")

    # Update with timer
    timer = machine.Timer(-1)
    timer.init(period=2000, mode=timer.PERIODIC, callback=lambda t: update())

    connect_wifi()

    if CONFIG["WEBSERVER_ENABLED"]:
        run_webserver()


main()
