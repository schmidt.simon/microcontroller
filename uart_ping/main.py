from machine import Pin, I2C
from machine import UART
import time

tx_pin = 23
rx_pin = 22


uart = UART(1, 9600)
uart.init(
    9600,
    bits=8,
    parity=None,
    stop=1,
    tx=tx_pin,
    rx=rx_pin,
)


def callback(x=None):
    print("In callback")

# Not in esp32...
# uart.irq(
#     UART.RX_ANY,
#     priority=1,
#     handler=callback,
# )


def start():
    # Don't run forever
    for _ in range(30):
        x = uart.read()
        if x is None:
            time.sleep_ms(100)
            continue
        print("Received", x)
        uart.write(x)

    print("Stopping")
