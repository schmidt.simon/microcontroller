EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF_Module:ESP32-WROOM-32 U?
U 1 1 5CD962C8
P 2000 3450
F 0 "U?" H 2000 5031 50  0000 C CNN
F 1 "ESP32-WROOM-32" H 2000 4940 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 2000 1950 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf" H 1700 3500 50  0001 C CNN
	1    2000 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 3850 2600 3850
$Comp
L MCU_ST_STM32F1:STM32F103C8Tx U?
U 1 1 5CD98D4D
P 5850 3450
F 0 "U?" H 5800 1861 50  0000 C CNN
F 1 "STM32F103C8Tx" H 5800 1770 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 5250 2050 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00161566.pdf" H 5850 3450 50  0001 C CNN
	1    5850 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 4350 3350 4350
Wire Wire Line
	3350 4350 3350 3850
Wire Wire Line
	2600 3750 3400 3750
Wire Wire Line
	6450 3350 6950 3350
Wire Wire Line
	3400 3750 3400 4250
Wire Wire Line
	3400 4250 5150 4250
$Comp
L Switch:SW_Push SW?
U 1 1 5CD9ED12
P 7150 3350
F 0 "SW?" H 7150 3635 50  0000 C CNN
F 1 "SW_Push" H 7150 3544 50  0000 C CNN
F 2 "" H 7150 3550 50  0001 C CNN
F 3 "~" H 7150 3550 50  0001 C CNN
	1    7150 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 3350 7350 3350
Wire Wire Line
	7700 3350 7700 3200
$Comp
L power:+3V3 #PWR?
U 1 1 5CDA0DE1
P 7700 3200
F 0 "#PWR?" H 7700 3050 50  0001 C CNN
F 1 "+3V3" H 7715 3373 50  0000 C CNN
F 2 "" H 7700 3200 50  0001 C CNN
F 3 "" H 7700 3200 50  0001 C CNN
	1    7700 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 4850 2000 5200
Wire Wire Line
	2000 5200 4000 5200
Wire Wire Line
	5750 5200 5750 4950
Wire Wire Line
	4000 5200 4000 5500
Connection ~ 4000 5200
Wire Wire Line
	4000 5200 5750 5200
$Comp
L power:GND #PWR?
U 1 1 5CDA5EED
P 4000 5500
F 0 "#PWR?" H 4000 5250 50  0001 C CNN
F 1 "GND" H 4005 5327 50  0000 C CNN
F 2 "" H 4000 5500 50  0001 C CNN
F 3 "" H 4000 5500 50  0001 C CNN
	1    4000 5500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
